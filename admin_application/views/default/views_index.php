<?php 
if (! defined('BASEPATH')) {
	exit('Access Denied');
}
?>

<!DOCTYPE HTML>
<html>
 <head>
  <title>后台管理系统</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/css/bui-min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/css/main-min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/js/jquery-1.8.1.min.js"></script>
 <script>
 $(function(){
	nav();
 });

 </script>
 </head>
 <body>

  <div class="header">
    
      <div class="dl-title">
       <!--<img src="/chinapost/Public/assets/img/top.png">-->
      </div>

    <div class="dl-log">欢迎您，<span class="dl-log-user"><?php echo $username ;?>所在组:<?php echo $group_name ;?></span>|<a href="#" id="btnShow"  class="dl-log-quit" >[修改密码]</a>|<a href="javascript:void(0)" title="退出系统" class="dl-log-quit" onclick="login_out()">[退出]</a>
    <div><a href="javascript:void(0);" onclick="cc()">saas</a></div>
	</div> 
  </div>
   <div class="content">
    <div class="dl-main-nav">
      <div class="dl-inform"><div class="dl-inform-title"><s class="dl-inform-icon dl-up"></s></div></div>
      <ul id="J_Nav"  class="nav-list ks-clear">
     			 <!-- 
        		<li class="nav-item dl-selected"><div class="nav-item-inner nav-home">系统管理</div></li>
        		<li class="nav-item dl-selected"><div class="nav-item-inner nav-order">业务管理</div></li>  
        		-->
        		<?php 
        			if(isset($list) && $list){
        				foreach($list as $l_k=>$l_v){
        					$selected= '' ;
        					if($l_k == 0){
        						$selected = 'dl-selected';
        					}
        			
        		?> 
        		 <li class="nav-item <?php echo $selected ;?>"><div class="nav-item-inner nav-order"><?php echo $l_v['name'];?></div></li>  
        		 <?php 
        		 		}
        		 	}	
        		 ?>     

      </ul>
    </div>
    <ul id="J_NavContent" class="dl-tab-conten">

    </ul>
   </div>
  
  <script type="text/javascript" src="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/js/bui-min.js"></script>
  <script type="text/javascript" src="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/js/common/main-min.js"></script>
  <script type="text/javascript" src="<?php echo  base_url() ;?>/<?php echo APPPATH?>/views/static/assets/js/config-min.js"></script>
  <script type="text/javascript" >
 	function nav(){
 	    BUI.use('common/main',function(){ 
 	 	 // new PageUtil.MainPage({
 	        //modulesConfig : config
 	       //});
		   //获取json
			$.getJSON('<?php echo site_url('common/get_menu');?>',function(config){	
				//console.dir(config);
				new PageUtil.MainPage({
					modulesConfig : config
				});
			 
			});
 	     });
	    
 	}  
 </script>
 </body>
</html>
 <script type="text/javascript">
var Overlay = BUI.Overlay
var dialog = new Overlay.Dialog({
	title:'修改密码',
	width:500,
	height:200,
	loader : {
		url : '<?php echo site_url('sys_admin/edit_passwd')?>',
		autoLoad : false, //不自动加载
		params : {"inajax":"1"},//附加的参数
		lazyLoad : false, //不延迟加载
		/*, //以下是默认选项
		dataType : 'text', //加载的数据类型
		property : 'bodyContent', //将加载的内容设置到对应的属性
		loadMask : {
		//el , dialog 的body
		},
		lazyLoad : {
		event : 'show', //显示的时候触发加载
		repeat : true //不重复加载
		},
		callback : function(text){
		var loader = this,
		target = loader.get('target'); //使用Loader的控件，此处是dialog
		//
		}
		*/
	},
	mask:false,
	success:function(){
		var passwd = $("#passwd").val() ;
		var repasswd = $("#repasswd").val() ;
		if(passwd == '' || repasswd == ''){
			BUI.Message.Alert('密码不可以为空','error');
			return false ;
		}else if(passwd != repasswd){
			BUI.Message.Alert('2次密码不相同','error');
			return false ;
		}
	var data_ = {
		'action':"doedit",
		'passwd':passwd,
		'repasswd':repasswd
	} ;	
	$.ajax({
		   type: "POST",
		   url: "<?php echo site_url("sys_admin/edit_passwd");?>" ,
		   data: data_,
		   cache:false,
		   dataType:"json",
		 //  async:false,
		   success: function(msg){
			var code = msg.resultcode ;
			var message = msg.resultinfo.errmsg ;
			if(code ==  1 ){
				window.location.href="<?php echo site_url("login/login_out");?>";
			}else if(code <0){
				BUI.Message.Alert('对不起没权限','error');
				return false ;
			}else{
				BUI.Message.Alert(message,'error');
				return false ;
			}
		   },
		   beforeSend:function(){
			  $("#result_").html('<font color="red"><img src="<?php echo base_url();?>/<?php echo APPPATH?>/views/static/Images/progressbar_microsoft.gif"></font>');
		   },
		   error:function(){
			   alert('服务器繁忙请稍。。。。');
		   }
		  
		});			
		 
		this.close();
	}
});
//dialog.show(); //是否自动显示
$('#btnShow').on('click',function () {
	dialog.show();
	dialog.get('loader').load();
});
function login_out(){
	 BUI.Message.Confirm('确定要退出系统吗?',function(){
		window.location.href="<?php echo site_url("login/login_out");?>";
	},'question');
	
}
</script>