<?php 
/*
 *系统基本信息配置
 *author 王建 
 */
if (! defined('BASEPATH')) {
    exit('Access Denied');
}
class Main extends MY_Controller {
	private $table_ ; //表的前缀
	function Main(){
		parent::__construct();
		$this->load->model('M_common');
		$this->table_ =table_pre('real_data');
	}
	function index(){
		
		$data = array(
			'group_name'=>group_name(),
		);
		$this->load->view(__TEMPLET_FOLDER__."/views_main",$data);			
	}

	
}




