<?php 
/*
 *后台首页文件
 *author 王建 
 */
if (! defined('BASEPATH')) {
    exit('Access Denied');
}
class Admin extends MY_Controller {
	private $table_ ; //表的前缀
	private $username ; //登录的用户名
	private $group_name = '' ;//所在的群组
	function Admin(){
		parent::__construct();
		$this->load->model('M_common');
		$this->table_ =table_pre('real_data');
		$this->username = login_name();
		$this->group_name = group_name() ;//所在的群组
	}
	//后台框架首页
	function index(){
		$list = $this->M_common->querylist("SELECT name from {$this->table_}common_admin_nav where pid = 0 and status = 1   order by disorder,id desc ");	
		
		$data = array(
			'list'=>$list,
			'username'=>$this->username,
			'group_name'=>"<font color='red'>".$this->group_name."</font>",
		);
		$this->load->view(__TEMPLET_FOLDER__."/views_index",$data);
	}
	
}
