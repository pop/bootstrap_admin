<?php
if (! defined('BASEPATH')) {
    exit('Access Denied');
}
class Category{
	function __construct() {
		
	}
	//格式化分类数据
	function format_category_data($data){
		$result = array() ;
		if(!$data){
			return ; 
		}
		foreach($data as $k=>$v){
			$result[$v['id']] = $v ;
		}
		$result = genTree9($result);
		$result = $this->getChildren($result) ;
		return $result ;
	}
	/**
	 * 格式化select
	 * @author 王建
	 * @param array $parent
	 * @deep int 层级关系 
	 * @return array
	 */
	function getChildren($parent,$deep=0) {
			foreach($parent as $row) {
				$data[] = array("id"=>isset($row['id'])?$row['id']:'', "name"=>isset($row['name'])?$row['name']:'',"pid"=>isset($row['pid'])?$row['pid']:'','deep'=>$deep,'typename'=>isset($row['typename'])?$row['typename']:'','disorder'=>isset($row['disorder'])?$row['disorder']:'','typeid'=>isset($row['typeid'])?$row['typeid']:'','status'=>isset($row['status'])?$row['status']:'');
				if (isset($row['children']) && !empty($row['children'])) {
					$data = array_merge($data, $this->getChildren($row['children'], $deep+1));
				}
			}
			return $data;
	}
	/**
	 *根据typeid生成缓存
	 * @author 王建
	 * @param int $typeid
	 * @return null
	 */
	function make_cache($typeid = ''){
		if(!$typeid){
			return ; 
		}
	}
}