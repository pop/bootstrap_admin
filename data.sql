/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : bootstrap_admin

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2014-05-03 11:08:42
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `57sy_common_admin_nav`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_admin_nav`;
CREATE TABLE `57sy_common_admin_nav` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '导航名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1开启 0关闭',
  `pid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '父级id',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '添加时间',
  `disorder` smallint(6) NOT NULL DEFAULT '0',
  `url` varchar(60) DEFAULT NULL COMMENT 'url地址',
  `url_type` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否是本站的地址 1是 0 不是',
  `collapsed` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否收缩，默认0 不收缩',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_admin_nav
-- ----------------------------
INSERT INTO 57sy_common_admin_nav VALUES ('21', '系统管理', '1', '0', '2014-02-03 17:29:36', '1', 'xit/cc/vvv/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('22', '模块管理', '1', '0', '2014-02-03 17:29:47', '12', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('23', '后台导航管理', '1', '21', '0000-00-00 00:00:00', '2', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('30', '导航列表', '1', '23', '0000-00-00 00:00:00', '1', 'nav/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('31', '导航添加', '1', '30', '0000-00-00 00:00:00', '13', 'nav/add/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('32', '网站用户管理', '1', '22', '2014-02-26 23:43:29', '2', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('33', '用户列表', '1', '32', '2014-02-26 23:43:51', '5', 'user/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('40', '导航编辑', '1', '30', '2014-02-28 00:57:00', '14', 'nav/edit/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('41', '用户编辑', '1', '33', '2014-02-28 09:59:14', '98', 'user/edit/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('52', '用户添加', '1', '33', '2014-02-28 10:34:17', '90', 'user/add/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('58', '角色配置', '1', '66', '2014-02-28 16:01:13', '7', 'role/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('59', '后台用户', '1', '66', '2014-03-01 00:59:50', '11', 'sys_admin/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('60', '后台用户添加', '1', '59', '2014-03-01 02:20:33', '4', 'sys_admin/add', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('61', '角色编辑', '1', '58', '2014-03-02 23:48:47', '3', 'role/edit/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('62', '后台用户编辑', '1', '59', '2014-03-02 23:50:01', '5', 'sys_admin/edit', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('64', '日志管理', '1', '21', '2014-03-03 03:45:48', '4', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('65', 'log列表', '1', '64', '2014-03-03 03:52:28', '90', 'log/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('66', '后台团队', '1', '21', '2014-03-03 20:52:59', '3', '', '1', '1');
INSERT INTO 57sy_common_admin_nav VALUES ('67', '网站基本信息', '1', '21', '2014-03-03 20:55:11', '1', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('68', '系统基本参数', '1', '67', '2014-03-03 20:56:28', '1', 'sysconfig/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('69', '系统参数添加', '1', '68', '2014-03-03 22:40:03', '2', 'sysconfig/add/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('70', '修改基本参数', '1', '68', '2014-03-04 00:33:16', '5', 'sysconfig/edit/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('71', '系统帮助', '1', '21', '2014-03-05 16:53:42', '10000', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('72', 'bug反馈', '1', '71', '2014-03-05 16:54:15', '1', 'http://www.57sy.com', '0', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('73', '欢迎页面', '1', '67', '2014-03-06 17:34:59', '0', 'main/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('74', '模型管理', '1', '21', '2014-04-22 14:59:24', '5', '', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('75', '联动模型', '1', '74', '2014-04-22 15:01:42', '1', 'category/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('76', '添加联动模型', '1', '75', '2014-04-22 16:55:13', '1', 'category/add/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('77', '编辑联动模型', '1', '75', '2014-04-22 16:55:36', '2', 'category/edit/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('78', '模型数据', '1', '74', '2014-04-23 13:35:00', '2', 'category_data/index/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('79', '添加模型数据', '1', '78', '2014-04-24 15:50:37', '1', 'category_data/add/', '1', '0');
INSERT INTO 57sy_common_admin_nav VALUES ('80', '编辑联动数据', '1', '78', '2014-04-24 15:51:02', '3', 'category_data/edit/', '1', '0');

-- ----------------------------
-- Table structure for `57sy_common_category_data`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_category_data`;
CREATE TABLE `57sy_common_category_data` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `pid` mediumint(8) unsigned NOT NULL,
  `typeid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `typename` varchar(30) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 开启 0 关闭',
  `disorder` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_category_data
-- ----------------------------
INSERT INTO 57sy_common_category_data VALUES ('58', '北京市', '0', '6', '城市级联框', '1', '6');
INSERT INTO 57sy_common_category_data VALUES ('59', '河南省', '0', '6', '城市级联框', '1', '1');
INSERT INTO 57sy_common_category_data VALUES ('60', '上海市', '0', '6', '城市级联框', '1', '2');
INSERT INTO 57sy_common_category_data VALUES ('61', '河北省', '0', '6', '城市级联框', '1', '4');
INSERT INTO 57sy_common_category_data VALUES ('62', '湖南省', '0', '6', '城市级联框', '1', '6');
INSERT INTO 57sy_common_category_data VALUES ('63', '东城区', '58', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('64', '西城区', '58', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('65', '朝阳区', '58', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('66', '海淀区', '58', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('67', '南阳市', '59', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('68', '洛阳市', '59', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('69', '周口市', '59', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('70', '淅川县', '67', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('71', '邓州市', '67', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('72', '宝山', '60', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('73', '广东省', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('74', '湖北省', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('75', '江苏省', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('76', '四川省', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('77', '43443', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('78', '4343', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('79', '23332', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('80', 'sdasd', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('81', '4434', '76', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('82', 'kjkjjkj', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('83', 'sdfsfsd', '0', '6', '城市级联框', '1', '0');
INSERT INTO 57sy_common_category_data VALUES ('84', 'sfdsf', '0', '6', '城市级联框', '1', '0');

-- ----------------------------
-- Table structure for `57sy_common_category_type`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_category_type`;
CREATE TABLE `57sy_common_category_type` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `cname` varchar(30) NOT NULL COMMENT '分类名称',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '添加日期',
  `addperson` varchar(30) NOT NULL COMMENT '添加人',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1开启 0 关闭',
  `modifytime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改日期',
  `remark` varchar(200) NOT NULL COMMENT '备注说明',
  PRIMARY KEY (`id`),
  KEY `cname` (`cname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_category_type
-- ----------------------------
INSERT INTO 57sy_common_category_type VALUES ('6', '城市级联框', '2014-04-25 10:35:07', 'wangjian', '1', '2014-04-27 12:25:58', '城市级联框描述信息');

-- ----------------------------
-- Table structure for `57sy_common_log_201402`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_log_201402`;
CREATE TABLE `57sy_common_log_201402` (
  `log_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `log_url` varchar(50) NOT NULL,
  `log_person` varchar(16) NOT NULL,
  `log_time` datetime NOT NULL,
  `log_ip` char(15) NOT NULL,
  `log_sql` varchar(255) NOT NULL,
  `log_status` tinyint(1) NOT NULL DEFAULT '1',
  `log_message` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_log_201402
-- ----------------------------
INSERT INTO 57sy_common_log_201402 VALUES ('1', 'role/edit', 'wangjian', '2014-03-03 03:20:26', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'娃娃亲\' , perm = \'a:7:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:16:\"sys_admin/index/\";i:4;s:8:\"dd/list/\";i:5;s:3:\"555\";i:6;s:7:\"6767677\";}\',status = \'0\' where id = \'4\'', '0', '修改角色为娃娃亲失败');
INSERT INTO 57sy_common_log_201402 VALUES ('2', 'role/edit', 'wangjian', '2014-03-03 03:20:39', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'娃娃亲\' , perm = \'a:7:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:16:\"sys_admin/index/\";i:4;s:8:\"dd/list/\";i:5;s:3:\"555\";i:6;s:7:\"6767677\";}\',status = \'1\' where id = \'4\'', '1', '修改角色为娃娃亲成功');
INSERT INTO 57sy_common_log_201402 VALUES ('3', 'role/add', 'wangjian', '2014-03-03 03:31:13', '127.0.0.1', 'INSERT INTO `57sy_common_role` (`rolename`, `status`, `addtime`) VALUES (\'oooo\', 1, \'2014-03-03 03:31:13\')', '1', '添加角色为oooo成功');
INSERT INTO 57sy_common_log_201402 VALUES ('4', 'role/add', 'wangjian', '2014-03-03 03:31:30', '127.0.0.1', 'INSERT INTO `57sy_common_role` (`rolename`, `status`, `addtime`) VALUES (\'据了解老两口考虑考虑&#039;\', 1, \'2014-03-03 03:31:30\')', '1', '添加角色为据了解老两口考虑考虑&#039;成功');
INSERT INTO 57sy_common_log_201402 VALUES ('5', 'role/edit', 'wangjian', '2014-03-03 03:31:48', '127.0.0.1', 'no sql', '0', '请选择权限');
INSERT INTO 57sy_common_log_201402 VALUES ('6', 'role/edit', 'taotao', '2014-03-03 03:31:58', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'据了解老两口\' , perm = \'a:1:{i:0;s:3:\"555\";}\',status = \'1\' where id = \'6\'', '1', '修改角色为据了解老两口成功');

-- ----------------------------
-- Table structure for `57sy_common_log_201403`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_log_201403`;
CREATE TABLE `57sy_common_log_201403` (
  `log_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `log_url` varchar(50) NOT NULL,
  `log_person` varchar(16) NOT NULL,
  `log_time` datetime NOT NULL,
  `log_ip` char(15) NOT NULL,
  `log_sql` varchar(255) NOT NULL,
  `log_status` tinyint(1) NOT NULL DEFAULT '1',
  `log_message` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_log_201403
-- ----------------------------
INSERT INTO 57sy_common_log_201403 VALUES ('1', 'role/edit', 'wangjian', '2014-03-03 03:20:26', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'娃娃亲\' , perm = \'a:7:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:16:\"sys_admin/index/\";i:4;s:8:\"dd/list/\";i:5;s:3:\"555\";i:6;s:7:\"6767677\";}\',status = \'0\' where id = \'4\'', '0', '修改角色为娃娃亲失败');
INSERT INTO 57sy_common_log_201403 VALUES ('2', 'role/edit', 'wangjian', '2014-03-03 03:20:39', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'娃娃亲\' , perm = \'a:7:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:16:\"sys_admin/index/\";i:4;s:8:\"dd/list/\";i:5;s:3:\"555\";i:6;s:7:\"6767677\";}\',status = \'1\' where id = \'4\'', '1', '修改角色为娃娃亲成功');
INSERT INTO 57sy_common_log_201403 VALUES ('3', 'role/add', 'wangjian', '2014-03-03 03:31:13', '127.0.0.1', 'INSERT INTO `57sy_common_role` (`rolename`, `status`, `addtime`) VALUES (\'oooo\', 1, \'2014-03-03 03:31:13\')', '1', '添加角色为oooo成功');
INSERT INTO 57sy_common_log_201403 VALUES ('4', 'role/add', 'wangjian', '2014-03-03 03:31:30', '127.0.0.1', 'INSERT INTO `57sy_common_role` (`rolename`, `status`, `addtime`) VALUES (\'据了解老两口考虑考虑&#039;\', 1, \'2014-03-03 03:31:30\')', '1', '添加角色为据了解老两口考虑考虑&#039;成功');
INSERT INTO 57sy_common_log_201403 VALUES ('5', 'role/edit', 'wangjian', '2014-03-03 03:31:48', '127.0.0.1', 'no sql', '0', '请选择权限');
INSERT INTO 57sy_common_log_201403 VALUES ('6', 'role/edit', 'wangjian', '2014-03-03 03:31:58', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'据了解老两口\' , perm = \'a:1:{i:0;s:3:\"555\";}\',status = \'1\' where id = \'6\'', '1', '修改角色为据了解老两口成功');
INSERT INTO 57sy_common_log_201403 VALUES ('7', 'role/edit', 'wangjian', '2014-03-03 03:32:32', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'据了解老两口\' , perm = \'a:2:{i:0;s:13:\"sys_admin/add\";i:1;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为据了解老两口成功');
INSERT INTO 57sy_common_log_201403 VALUES ('8', 'role/edit', 'wangjian', '2014-03-03 03:32:47', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'据了解老两口1\' , perm = \'a:2:{i:0;s:13:\"sys_admin/add\";i:1;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为据了解老两口1成功');
INSERT INTO 57sy_common_log_201403 VALUES ('9', 'role/edit', 'wangjian', '2014-03-03 03:33:18', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'12\' , perm = \'a:2:{i:0;s:13:\"sys_admin/add\";i:1;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为12成功');
INSERT INTO 57sy_common_log_201403 VALUES ('10', 'sys_admin/add', 'wangjian', '2014-03-03 03:35:51', '127.0.0.1', 'INSERT INTO `57sy_common_system_user` (`username`, `passwd`, `status`, `addtime`, `gid`, `super_admin`) VALUES (\'5567777\', \'3d2172418ce305c7d16d4b05597c6a59\', 1, \'2014-03-03 03:35:51\', 4, 0)', '1', '添加用户为5567777成功');
INSERT INTO 57sy_common_log_201403 VALUES ('11', 'sys_admin/edit', 'wangjian', '2014-03-03 03:37:11', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'0\' , gid = \'4\', perm = \'\' ,username = \'5567777\',super_admin = \'0\' where id = \'13\'', '1', '修改用户为5567777成功');
INSERT INTO 57sy_common_log_201403 VALUES ('12', 'sys_admin/edit', 'wangjian', '2014-03-03 03:37:20', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'0\' , gid = \'4\', perm = \'\' ,username = \'5567777\',super_admin = \'0\' where id = \'13\'', '0', '修改用户为5567777失败');
INSERT INTO 57sy_common_log_201403 VALUES ('13', 'login/dologin', 'wangjian', '2014-03-03 03:41:41', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('14', 'nav/add', 'wangjian', '2014-03-03 03:45:48', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (21, \'日志管理\', 1, \'2014-03-03 03:45:48\', \'\', 12)', '1', '添加导航日志管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('15', 'nav/add', 'wangjian', '2014-03-03 03:52:28', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (64, \'log列表\', 1, \'2014-03-03 03:52:28\', \'\', 90)', '1', '添加导航log列表成功');
INSERT INTO 57sy_common_log_201403 VALUES ('16', 'nav/edit', 'wangjian', '2014-03-03 03:52:47', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'64\',name = \'log列表\',url = \'log/index/\' ,disorder = \'90\' ,status = \'1\' where id = \'65\'', '1', '编辑导航log列表成功');
INSERT INTO 57sy_common_log_201403 VALUES ('17', 'nav/edit', 'wangjian', '2014-03-03 04:37:27', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'33\',name = \'业务添加\',url = \'6767677\' ,disorder = \'90\' ,status = \'1\' where id = \'52\'', '0', '编辑导航业务添加失败');
INSERT INTO 57sy_common_log_201403 VALUES ('18', 'login/dologin', 'taotao', '2014-03-03 04:38:39', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('19', 'login/dologin', 'wangjian', '2014-03-03 04:38:50', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('20', 'sys_admin/edit', 'wangjian', '2014-03-03 04:39:12', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:3:{i:0;s:3:\"555\";i:1;s:7:\"6767677\";i:2;s:10:\"log/index/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201403 VALUES ('21', 'login/dologin', 'taotao', '2014-03-03 04:39:18', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('22', 'nav/add', 'taotao', '2014-03-03 04:39:53', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (0, \'1233\', 1, \'2014-03-03 04:39:53\', \'\', 122)', '1', '添加导航1233成功');
INSERT INTO 57sy_common_log_201403 VALUES ('23', 'nav/add', 'taotao', '2014-03-03 04:40:19', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (66, \'oooo\', 1, \'2014-03-03 04:40:19\', \'\', 99)', '1', '添加导航oooo成功');
INSERT INTO 57sy_common_log_201403 VALUES ('24', 'login/dologin', 'wangjian', '2014-03-03 20:45:46', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('25', 'nav/add', 'wangjian', '2014-03-03 20:52:59', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (21, \'后台团队\', 1, \'2014-03-03 20:52:59\', \'\', 9)', '1', '添加导航后台团队成功');
INSERT INTO 57sy_common_log_201403 VALUES ('26', 'nav/edit', 'wangjian', '2014-03-03 20:53:20', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'66\',name = \'角色配置\',url = \'role/index/\' ,disorder = \'7\' ,status = \'1\' where id = \'58\'', '1', '编辑导航角色配置成功');
INSERT INTO 57sy_common_log_201403 VALUES ('27', 'nav/edit', 'wangjian', '2014-03-03 20:53:53', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'66\',name = \'后台用户\',url = \'sys_admin/index/\' ,disorder = \'11\' ,status = \'1\' where id = \'59\'', '1', '编辑导航后台用户成功');
INSERT INTO 57sy_common_log_201403 VALUES ('28', 'nav/add', 'wangjian', '2014-03-03 20:55:11', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (21, \'网站基本信息\', 1, \'2014-03-03 20:55:11\', \'\', 1)', '1', '添加导航网站基本信息成功');
INSERT INTO 57sy_common_log_201403 VALUES ('29', 'nav/add', 'wangjian', '2014-03-03 20:56:28', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (67, \'系统基本参数\', 1, \'2014-03-03 20:56:28\', \'\', 1)', '1', '添加导航系统基本参数成功');
INSERT INTO 57sy_common_log_201403 VALUES ('30', 'nav/edit', 'wangjian', '2014-03-03 20:59:29', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'67\',name = \'系统基本参数\',url = \'sysconfig/index/\' ,disorder = \'1\' ,status = \'1\' where id = \'68\'', '1', '编辑导航系统基本参数成功');
INSERT INTO 57sy_common_log_201403 VALUES ('31', 'nav/add', 'wangjian', '2014-03-03 22:40:04', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (68, \'系统参数添加\', 1, \'2014-03-03 22:40:03\', \'sysconfig/add/\', 2)', '1', '添加导航系统参数添加成功');
INSERT INTO 57sy_common_log_201403 VALUES ('32', 'sysconfig/add', 'wangjian', '2014-03-03 22:50:52', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'\', \'\', \'\', \'string\', 0)', '1', '添加系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('33', 'sysconfig/add', 'wangjian', '2014-03-03 22:54:52', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'tyyyy\', \'sdasdas\', \'\', \'textarea\', 3)', '1', '添加系统变量tyyyy成功');
INSERT INTO 57sy_common_log_201403 VALUES ('34', 'sysconfig/add', 'wangjian', '2014-03-03 22:56:06', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'wangjain\', \'wwwada\', \'\', \'string\', 1)', '1', '添加系统变量wangjain成功');
INSERT INTO 57sy_common_log_201403 VALUES ('35', 'sysconfig/add', 'wangjian', '2014-03-03 22:58:13', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'asdasd3\', \'23323\', \'sdssdas\', \'string\', 1)', '1', '添加系统变量asdasd3成功');
INSERT INTO 57sy_common_log_201403 VALUES ('36', 'sysconfig/add', 'wangjian', '2014-03-03 23:35:29', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'hhhh\', \'uuyuu\', \'rrtt\', \'string\', 1)', '1', '添加系统变量hhhh成功');
INSERT INTO 57sy_common_log_201403 VALUES ('37', 'sysconfig/add', 'wangjian', '2014-03-04 00:14:15', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_max\', \'200\', \'上传图片的大小\', \'string\', 2)', '1', '添加系统变量cfg_max成功');
INSERT INTO 57sy_common_log_201403 VALUES ('38', 'sysconfig/add', 'wangjian', '2014-03-04 00:24:59', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'rtyyy\', \'wangjian\', \'1233\', \'string\', 1)', '1', '添加系统变量rtyyy成功');
INSERT INTO 57sy_common_log_201403 VALUES ('39', 'sysconfig/add', 'wangjian', '2014-03-04 00:28:05', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_isopen\', \'Y\', \'系统是否关闭\', \'boolean\', 1)', '1', '添加系统变量cfg_isopen成功');
INSERT INTO 57sy_common_log_201403 VALUES ('40', 'sysconfig/add', 'wangjian', '2014-03-04 00:28:13', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'Array\', \'\', \'\', \'string\', 0)', '1', '添加系统变量Array成功');
INSERT INTO 57sy_common_log_201403 VALUES ('41', 'nav/add', 'wangjian', '2014-03-04 00:33:16', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (68, \'修改基本参数\', 1, \'2014-03-04 00:33:16\', \'sysconfig/edit/\', 5)', '1', '添加导航修改基本参数成功');
INSERT INTO 57sy_common_log_201403 VALUES ('42', 'sysconfig/edit', 'wangjian', '2014-03-04 00:45:31', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('43', 'sysconfig/edit', 'wangjian', '2014-03-04 00:46:01', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('44', 'sysconfig/edit', 'wangjian', '2014-03-04 00:46:13', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('45', 'sysconfig/edit', 'wangjian', '2014-03-04 00:46:19', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('46', 'sysconfig/edit', 'wangjian', '2014-03-04 00:47:58', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('47', 'sysconfig/edit', 'wangjian', '2014-03-04 00:48:03', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('48', 'sysconfig/edit', 'wangjian', '2014-03-04 00:48:16', '127.0.0.1', 'sysconfig_update', '0', '修改系统变量失败');
INSERT INTO 57sy_common_log_201403 VALUES ('49', 'sysconfig/add', 'wangjian', '2014-03-04 00:48:45', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cdf_install\', \'install/\', \'DedeCMS安装目录\', \'string\', 1)', '1', '添加系统变量cdf_install成功');
INSERT INTO 57sy_common_log_201403 VALUES ('50', 'sysconfig/add', 'wangjian', '2014-03-04 00:49:19', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_cookie_encode\', \'UsSQu3494J\', \'cookie的加密密码rn\', \'string\', 1)', '1', '添加系统变量cfg_cookie_encode成功');
INSERT INTO 57sy_common_log_201403 VALUES ('51', 'sysconfig/add', 'wangjian', '2014-03-04 00:49:51', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_tu\', \'123\', \'缩略图默认宽度\', \'string\', 3)', '1', '添加系统变量cfg_tu成功');
INSERT INTO 57sy_common_log_201403 VALUES ('52', 'sysconfig/add', 'wangjian', '2014-03-04 00:50:29', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_isopen_huiyuan\', \'Y\', \'是否开启会员功能\', \'boolean\', 2)', '1', '添加系统变量cfg_isopen_huiyuan成功');
INSERT INTO 57sy_common_log_201403 VALUES ('53', 'sysconfig/edit', 'wangjian', '2014-03-04 00:50:35', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('54', 'sysconfig/edit', 'wangjian', '2014-03-04 00:53:14', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('55', 'sysconfig/edit', 'wangjian', '2014-03-04 00:53:23', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('56', 'sysconfig/add', 'wangjian', '2014-03-04 00:53:50', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'des\', \'我的站点\', \'站点描述rn\', \'textarea\', 1)', '1', '添加系统变量des成功');
INSERT INTO 57sy_common_log_201403 VALUES ('57', 'sysconfig/add', 'wangjian', '2014-03-04 00:54:20', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_zhian_open\', \'Y\', \'站点是否开启\', \'string\', 1)', '1', '添加系统变量cfg_zhian_open成功');
INSERT INTO 57sy_common_log_201403 VALUES ('58', 'sysconfig/edit', 'wangjian', '2014-03-04 00:54:40', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('59', 'sysconfig/add', 'wangjian', '2014-03-04 01:05:12', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'4343344\', \'3443\', \'4343\', \'boolean\', 2)', '1', '添加系统变量4343344成功');
INSERT INTO 57sy_common_log_201403 VALUES ('60', 'sysconfig/edit', 'wangjian', '2014-03-04 01:10:09', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('61', 'role/edit', 'wangjian', '2014-03-04 01:10:37', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管里\' , perm = \'a:7:{i:0;s:16:\"sysconfig/index/\";i:1;s:14:\"sysconfig/add/\";i:2;s:10:\"nav/index/\";i:3;s:8:\"nav/add/\";i:4;s:16:\"sys_admin/index/\";i:5;s:13:\"sys_admin/add\";i:6;s:14:\"sys_admin/edit\";}\',status = \'1\' wher', '1', '修改角色为系统管里成功');
INSERT INTO 57sy_common_log_201403 VALUES ('62', 'login/dologin', 'taotao', '2014-03-04 01:10:44', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('63', 'sysconfig/add', 'taotao', '2014-03-04 01:11:36', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'asas\', \'adas\', \'adasdad\', \'string\', 1)', '1', '添加系统变量asas成功');
INSERT INTO 57sy_common_log_201403 VALUES ('64', 'sysconfig/add', 'taotao', '2014-03-04 01:12:02', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'3232\', \'N\', \'2323\', \'boolean\', 1)', '1', '添加系统变量3232成功');
INSERT INTO 57sy_common_log_201403 VALUES ('65', 'login/dologin', 'wangjian', '2014-03-04 01:12:47', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('66', 'role/edit', 'wangjian', '2014-03-04 01:13:28', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:7:{i:0;s:16:\"sysconfig/index/\";i:1;s:14:\"sysconfig/add/\";i:2;s:10:\"nav/index/\";i:3;s:8:\"nav/add/\";i:4;s:16:\"sys_admin/index/\";i:5;s:13:\"sys_admin/add\";i:6;s:14:\"sys_admin/edit\";}\',status = \'1\' wher', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('67', 'login/dologin', 'taotao', '2014-03-04 01:13:34', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('68', 'login/dologin', 'wangjian', '2014-03-04 01:14:51', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('69', 'role/edit', 'wangjian', '2014-03-04 01:15:08', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:6:{i:0;s:16:\"sysconfig/index/\";i:1;s:14:\"sysconfig/add/\";i:2;s:10:\"nav/index/\";i:3;s:16:\"sys_admin/index/\";i:4;s:13:\"sys_admin/add\";i:5;s:14:\"sys_admin/edit\";}\',status = \'1\' where id = \'1\'', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('70', 'sys_admin/edit', 'wangjian', '2014-03-04 01:15:31', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201403 VALUES ('71', 'login/dologin', 'taotao', '2014-03-04 01:15:38', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('72', 'sysconfig/add', 'taotao', '2014-03-04 01:15:50', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'1233\', \'ccccc\', \'ccccac\', \'string\', 1)', '1', '添加系统变量1233成功');
INSERT INTO 57sy_common_log_201403 VALUES ('73', 'login/dologin', 'wangjian', '2014-03-04 01:16:04', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('74', 'role/edit', 'wangjian', '2014-03-04 01:16:25', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:5:{i:0;s:16:\"sysconfig/index/\";i:1;s:10:\"nav/index/\";i:2;s:16:\"sys_admin/index/\";i:3;s:13:\"sys_admin/add\";i:4;s:14:\"sys_admin/edit\";}\',status = \'1\' where id = \'1\'', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('75', 'login/dologin', 'taotao', '2014-03-04 01:16:35', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('76', 'login/dologin', 'wangjian', '2014-03-04 01:17:25', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('77', 'login/dologin', 'wangjian', '2014-03-04 21:07:44', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('78', 'sysconfig/edit', 'wangjian', '2014-03-04 21:10:10', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('79', 'sysconfig/edit', 'wangjian', '2014-03-04 21:10:15', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('80', 'sysconfig/add', 'wangjian', '2014-03-04 21:27:03', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'yyuuu\', \'1233\', \'4434343\', \'number\', 1)', '1', '添加系统变量yyuuu成功');
INSERT INTO 57sy_common_log_201403 VALUES ('81', 'sysconfig/add', 'wangjian', '2014-03-04 21:32:55', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_pin\', \'100\', \'么次访问的次数rn\', \'string\', 1)', '1', '添加系统变量cfg_pin成功');
INSERT INTO 57sy_common_log_201403 VALUES ('82', 'sysconfig/add', 'wangjian', '2014-03-04 21:34:46', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_memcsche\', \'Y\', \'是否开启memcache缓存\', \'boolean\', 3)', '1', '添加系统变量cfg_memcsche成功');
INSERT INTO 57sy_common_log_201403 VALUES ('83', 'sysconfig/add', 'wangjian', '2014-03-04 21:37:14', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg-xingneng\', \'120\', \'性能值\', \'number\', 3)', '1', '添加系统变量cfg-xingneng成功');
INSERT INTO 57sy_common_log_201403 VALUES ('84', 'sysconfig/edit', 'wangjian', '2014-03-04 21:37:22', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('85', 'sysconfig/edit', 'wangjian', '2014-03-04 22:02:59', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('86', 'sysconfig/add', 'wangjian', '2014-03-04 22:03:23', '127.0.0.1', 'INSERT INTO `57sy_common_sysconfig` (`varname`, `value`, `info`, `type`, `groupid`) VALUES (\'cfg_ssa_\', \'爱的\', \'我的撒旦\', \'string\', 1)', '1', '添加系统变量cfg_ssa_成功');
INSERT INTO 57sy_common_log_201403 VALUES ('87', 'login/dologin', 'wangjian', '2014-03-05 13:13:31', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('88', 'login/dologin', 'wangjian', '2014-03-05 16:22:24', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('89', 'login/dologin', 'wangjian', '2014-03-05 16:28:00', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('90', 'nav/edit', 'wangjian', '2014-03-05 16:38:12', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'0\',name = \'模块管理\',url = \'\' ,disorder = \'12\' ,status = \'1\' where id = \'22\'', '1', '编辑导航模块管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('91', 'nav/add', 'wangjian', '2014-03-05 16:53:42', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (21, \'系统帮助\', 1, \'2014-03-05 16:53:42\', \'\', 10000)', '1', '添加导航系统帮助成功');
INSERT INTO 57sy_common_log_201403 VALUES ('92', 'nav/add', 'wangjian', '2014-03-05 16:54:15', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (71, \'bug反馈\', 1, \'2014-03-05 16:54:15\', \'http:://www.57sy.com\', 1)', '1', '添加导航bug反馈成功');
INSERT INTO 57sy_common_log_201403 VALUES ('93', 'nav/edit', 'wangjian', '2014-03-05 17:01:33', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'71\',name = \'bug反馈\',url = \'http:://www.57sy.com\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'0\' where id = \'72\'', '0', '编辑导航bug反馈失败');
INSERT INTO 57sy_common_log_201403 VALUES ('94', 'nav/edit', 'wangjian', '2014-03-05 17:02:18', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'71\',name = \'bug反馈\',url = \'http:://www.57sy.com\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'0\' where id = \'72\'', '0', '编辑导航bug反馈失败');
INSERT INTO 57sy_common_log_201403 VALUES ('95', 'nav/edit', 'wangjian', '2014-03-05 17:03:16', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'71\',name = \'bug反馈\',url = \'http:://www.57sy.com\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'1\' where id = \'72\'', '1', '编辑导航bug反馈成功');
INSERT INTO 57sy_common_log_201403 VALUES ('96', 'nav/edit', 'wangjian', '2014-03-05 17:11:03', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'71\',name = \'bug反馈\',url = \'http://www.57sy.com\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'0\' where id = \'72\'', '1', '编辑导航bug反馈成功');
INSERT INTO 57sy_common_log_201403 VALUES ('97', 'login/dologin', 'wangjian', '2014-03-06 17:24:21', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('98', 'login/dologin', 'wangjian', '2014-03-06 17:25:01', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('99', 'nav/edit', 'wangjian', '2014-03-06 17:30:11', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'后台导航管理\',url = \'\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'1\' where id = \'23\'', '1', '编辑导航后台导航管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('100', 'nav/add', 'wangjian', '2014-03-06 17:34:59', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (67, \'欢迎页面\', 1, \'2014-03-06 17:34:59\', \'\', 0)', '1', '添加导航欢迎页面成功');
INSERT INTO 57sy_common_log_201403 VALUES ('101', 'nav/edit', 'wangjian', '2014-03-06 17:35:34', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'67\',name = \'欢迎页面\',url = \'main/index/\' ,disorder = \'0\' ,status = \'1\' ,url_type = \'1\' where id = \'73\'', '1', '编辑导航欢迎页面成功');
INSERT INTO 57sy_common_log_201403 VALUES ('102', 'login/dologin', 'wangjian', '2014-03-09 18:05:22', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('103', 'login/dologin', 'wangjian', '2014-03-12 21:04:43', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('104', 'login/dologin', 'wangjian', '2014-03-19 17:54:01', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('105', 'sys_admin/edit', 'wangjian', '2014-03-19 17:55:46', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'0\' , gid = \'4\', perm = \'\' ,username = \'ewew\',super_admin = \'0\' where id = \'2\'', '1', '修改用户为ewew成功');
INSERT INTO 57sy_common_log_201403 VALUES ('106', 'login/dologin', 'taotao', '2014-03-19 17:56:05', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('107', 'login/dologin', 'taotao', '2014-03-19 18:01:00', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('108', 'login/dologin', 'wangjian', '2014-03-19 18:06:33', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('109', 'role/edit', 'wangjian', '2014-03-19 18:07:05', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:7:{i:0;s:16:\"sysconfig/index/\";i:1;s:14:\"sysconfig/add/\";i:2;s:15:\"sysconfig/edit/\";i:3;s:10:\"nav/index/\";i:4;s:16:\"sys_admin/index/\";i:5;s:13:\"sys_admin/add\";i:6;s:14:\"sys_admin/edit\";}\',status = ', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('110', 'login/dologin', 'taotao', '2014-03-19 18:07:35', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('111', 'login/dologin', 'wangjian', '2014-03-19 18:07:49', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('112', 'role/edit', 'wangjian', '2014-03-19 18:08:36', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:8:{i:0;s:11:\"main/index/\";i:1;s:16:\"sysconfig/index/\";i:2;s:14:\"sysconfig/add/\";i:3;s:15:\"sysconfig/edit/\";i:4;s:10:\"nav/index/\";i:5;s:16:\"sys_admin/index/\";i:6;s:13:\"sys_admin/add\";i:7;s:14:\"sys_a', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('113', 'sys_admin/edit', 'wangjian', '2014-03-19 18:08:59', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201403 VALUES ('114', 'login/dologin', 'taotao', '2014-03-19 18:09:05', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('115', 'login/dologin', 'wangjian', '2014-03-19 18:10:26', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('116', 'login/dologin', 'taotao', '2014-03-19 18:11:20', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('117', 'login/dologin', 'wangjian', '2014-03-19 18:16:38', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('118', 'role/edit', 'wangjian', '2014-03-19 18:17:00', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:3:{i:0;s:11:\"main/index/\";i:1;s:16:\"sysconfig/index/\";i:2;s:14:\"sysconfig/add/\";}\',status = \'1\' where id = \'1\'', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201403 VALUES ('119', 'login/dologin', 'taotao', '2014-03-19 18:17:10', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('120', 'login/dologin', 'wangjian', '2014-03-19 18:17:25', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('121', 'sys_admin/edit', 'wangjian', '2014-03-19 18:17:47', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201403 VALUES ('122', 'login/dologin', 'taotao', '2014-03-19 18:17:53', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('123', 'sysconfig/edit', 'taotao', '2014-03-19 18:17:57', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('124', 'login/dologin', 'wangjian', '2014-03-19 18:22:32', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('125', 'login/dologin', 'wangjian', '2014-03-19 18:41:42', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('126', 'login/dologin', 'wangjian', '2014-03-20 17:02:14', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('127', 'sysconfig/edit', 'wangjian', '2014-03-20 17:32:18', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201403 VALUES ('128', 'login/dologin', 'wangjian', '2014-03-22 22:28:32', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('129', 'login/dologin', 'wangjian', '2014-03-22 22:32:17', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('130', 'login/dologin', 'wangjian', '2014-03-22 22:32:29', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('131', 'login/dologin', 'wangjian', '2014-03-22 22:45:28', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('132', 'login/dologin', 'wangjian', '2014-03-22 22:46:14', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201403 VALUES ('133', 'login/dologin', 'wangjian', '2014-03-22 22:55:26', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');

-- ----------------------------
-- Table structure for `57sy_common_log_201404`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_log_201404`;
CREATE TABLE `57sy_common_log_201404` (
  `log_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `log_url` varchar(50) NOT NULL,
  `log_person` varchar(16) NOT NULL,
  `log_time` datetime NOT NULL,
  `log_ip` char(15) NOT NULL,
  `log_sql` varchar(255) NOT NULL,
  `log_status` tinyint(1) NOT NULL DEFAULT '1',
  `log_message` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_log_201404
-- ----------------------------
INSERT INTO 57sy_common_log_201404 VALUES ('1', 'login/dologin', 'wangjian', '2014-04-03 13:59:40', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('2', 'login/dologin', 'wangjian', '2014-04-10 11:02:26', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('3', 'login/dologin', 'wangjian', '2014-04-22 14:50:40', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('4', 'nav/add', 'wangjian', '2014-04-22 14:59:24', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (21, \'模型管理\', 1, \'2014-04-22 14:59:24\', \'\', 7)', '1', '添加导航模型管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('5', 'nav/edit', 'wangjian', '2014-04-22 14:59:57', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'后台导航管理\',url = \'\' ,disorder = \'2\' ,status = \'1\' ,url_type = \'1\' where id = \'23\'', '1', '编辑导航后台导航管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('6', 'nav/edit', 'wangjian', '2014-04-22 15:00:36', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'后台团队\',url = \'\' ,disorder = \'3\' ,status = \'1\' ,url_type = \'1\' where id = \'66\'', '1', '编辑导航后台团队成功');
INSERT INTO 57sy_common_log_201404 VALUES ('7', 'nav/edit', 'wangjian', '2014-04-22 15:00:46', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'日志管理\',url = \'\' ,disorder = \'4\' ,status = \'1\' ,url_type = \'1\' where id = \'64\'', '1', '编辑导航日志管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('8', 'nav/edit', 'wangjian', '2014-04-22 15:00:59', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'模型管理\',url = \'\' ,disorder = \'5\' ,status = \'1\' ,url_type = \'1\' where id = \'74\'', '1', '编辑导航模型管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('9', 'nav/add', 'wangjian', '2014-04-22 15:01:42', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (74, \'联动模型\', 1, \'2014-04-22 15:01:42\', \'category/index/\', 1)', '1', '添加导航联动模型成功');
INSERT INTO 57sy_common_log_201404 VALUES ('10', 'category/add', 'wangjian', '2014-04-22 15:19:09', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`) VALUES (\'城市级联框\', 1, \'2014-04-22 15:19:09\', \'wangjian\', \'2014-04-22 15:19:09\')', '1', '添加模型为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('11', 'category/add', 'wangjian', '2014-04-22 15:23:24', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`) VALUES (\'百灵游戏\', 1, \'2014-04-22 15:23:24\', \'wangjian\', \'2014-04-22 15:23:24\')', '1', '添加模型为百灵游戏成功');
INSERT INTO 57sy_common_log_201404 VALUES ('12', 'category/edit', 'wangjian', '2014-04-22 15:39:15', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'343443\' where id = \'2\'', '1', '修改名称为343443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('13', 'category/edit', 'wangjian', '2014-04-22 15:40:52', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'55454\',modifytime = \'2014-04-22 15:40:52\'  where id = \'2\'', '1', '修改名称为55454成功');
INSERT INTO 57sy_common_log_201404 VALUES ('14', 'category/edit', 'wangjian', '2014-04-22 15:41:04', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'0\',cname = \'55454\',modifytime = \'2014-04-22 15:41:04\'  where id = \'2\'', '1', '修改名称为55454成功');
INSERT INTO 57sy_common_log_201404 VALUES ('15', 'category/edit', 'wangjian', '2014-04-22 16:39:14', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'0\',cname = \'55454\',modifytime = \'2014-04-22 16:39:14\'  where id = \'2\'', '1', '修改名称为55454成功');
INSERT INTO 57sy_common_log_201404 VALUES ('16', 'category/add', 'wangjian', '2014-04-22 16:42:25', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`) VALUES (\'mygame\', 1, \'2014-04-22 16:42:25\', \'wangjian\', \'2014-04-22 16:42:25\')', '1', '添加模型为mygame成功');
INSERT INTO 57sy_common_log_201404 VALUES ('17', 'category/edit', 'wangjian', '2014-04-22 16:42:41', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'mygame1\',modifytime = \'2014-04-22 16:42:41\'  where id = \'3\'', '1', '修改名称为mygame1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('18', 'category/edit', 'wangjian', '2014-04-22 16:43:50', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'mygame1\',modifytime = \'2014-04-22 16:43:50\'  where id = \'3\'', '1', '修改名称为mygame1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('19', 'category/edit', 'wangjian', '2014-04-22 16:43:59', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'0\',cname = \'mygame15\',modifytime = \'2014-04-22 16:43:59\'  where id = \'3\'', '1', '修改名称为mygame15成功');
INSERT INTO 57sy_common_log_201404 VALUES ('20', 'category/add', 'wangjian', '2014-04-22 16:47:05', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`, `remark`) VALUES (\'5566\', 1, \'2014-04-22 16:47:05\', \'wangjian\', \'2014-04-22 16:47:05\', \'65565665\')', '1', '添加模型为5566成功');
INSERT INTO 57sy_common_log_201404 VALUES ('21', 'category/edit', 'wangjian', '2014-04-22 16:48:25', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'5566\',modifytime = \'2014-04-22 16:48:25\',remark = \'322323\'   where id = \'4\'', '1', '修改名称为5566成功');
INSERT INTO 57sy_common_log_201404 VALUES ('22', 'category/edit', 'wangjian', '2014-04-22 16:48:31', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'5566\',modifytime = \'2014-04-22 16:48:31\',remark = \'反反复复\'   where id = \'4\'', '1', '修改名称为5566成功');
INSERT INTO 57sy_common_log_201404 VALUES ('23', 'category/edit', 'wangjian', '2014-04-22 16:53:58', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'mygame15\',modifytime = \'2014-04-22 16:53:58\',remark = \'通通通\'   where id = \'3\'', '1', '修改名称为mygame15成功');
INSERT INTO 57sy_common_log_201404 VALUES ('24', 'nav/add', 'wangjian', '2014-04-22 16:55:13', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (75, \'添加联动模型\', 1, \'2014-04-22 16:55:13\', \'category/add/\', 1)', '1', '添加导航添加联动模型成功');
INSERT INTO 57sy_common_log_201404 VALUES ('25', 'nav/add', 'wangjian', '2014-04-22 16:55:36', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (75, \'编辑联动模型\', 1, \'2014-04-22 16:55:36\', \'category/edit/\', 2)', '1', '添加导航编辑联动模型成功');
INSERT INTO 57sy_common_log_201404 VALUES ('26', 'role/edit', 'wangjian', '2014-04-22 16:56:21', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:5:{i:0;s:11:\"main/index/\";i:1;s:16:\"sysconfig/index/\";i:2;s:14:\"sysconfig/add/\";i:3;s:15:\"category/index/\";i:4;s:13:\"category/add/\";}\',status = \'1\' where id = \'1\'', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('27', 'sys_admin/edit', 'wangjian', '2014-04-22 16:56:56', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , passwd = \'44bc64daf3c37a7ada23e63a137310bd\', gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '0', '修改用户为taotao失败');
INSERT INTO 57sy_common_log_201404 VALUES ('28', 'sys_admin/edit', 'wangjian', '2014-04-22 16:57:16', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , passwd = \'44bc64daf3c37a7ada23e63a137310bd\', gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '0', '修改用户为taotao失败');
INSERT INTO 57sy_common_log_201404 VALUES ('29', 'login/dologin', 'taotao', '2014-04-22 16:57:43', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('30', 'category/add', 'taotao', '2014-04-22 16:58:26', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`, `remark`) VALUES (\'dasd\', 1, \'2014-04-22 16:58:26\', \'taotao\', \'2014-04-22 16:58:26\', \'asdsa\')', '1', '添加模型为dasd成功');
INSERT INTO 57sy_common_log_201404 VALUES ('31', 'sys_admin/edit', 'wangjian', '2014-04-22 16:59:15', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '0', '修改用户为taotao失败');
INSERT INTO 57sy_common_log_201404 VALUES ('32', 'sys_admin/edit', 'wangjian', '2014-04-22 17:00:06', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , passwd = \'44bc64daf3c37a7ada23e63a137310bd\', gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '0', '修改用户为taotao失败');
INSERT INTO 57sy_common_log_201404 VALUES ('33', 'sys_admin/edit', 'wangjian', '2014-04-22 17:00:11', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , passwd = \'20313dbba330959d4c95368fc1049d2e\', gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201404 VALUES ('34', 'sys_admin/edit', 'wangjian', '2014-04-22 17:00:20', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , passwd = \'44bc64daf3c37a7ada23e63a137310bd\', gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201404 VALUES ('35', 'sysconfig/edit', 'wangjian', '2014-04-22 17:09:14', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('36', 'sysconfig/edit', 'wangjian', '2014-04-22 17:09:23', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('37', 'login/dologin', 'wangjian', '2014-04-23 11:45:13', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('38', 'login/dologin', 'wangjian', '2014-04-23 13:29:54', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('39', 'nav/add', 'wangjian', '2014-04-23 13:35:00', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (74, \'模型数据\', 1, \'2014-04-23 13:35:00\', \'category_data/index/\', 2)', '1', '添加导航模型数据成功');
INSERT INTO 57sy_common_log_201404 VALUES ('40', 'category_data/add', 'wangjian', '2014-04-23 15:26:43', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'海淀1\', 1, 1, 2, 1, \'城市级联框\')', '1', '添加数据为海淀1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('41', 'category_data/add', 'wangjian', '2014-04-23 15:26:58', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'海淀1-1\', 1, 3, 7, 1, \'城市级联框\')', '1', '添加数据为海淀1-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('42', 'category_data/add', 'wangjian', '2014-04-23 15:27:10', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'海淀1-1-1\', 1, 0, 8, 1, \'城市级联框\')', '1', '添加数据为海淀1-1-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('43', 'category_data/add', 'wangjian', '2014-04-23 15:31:03', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'太太太太\', 1, 35, 0, 1, \'城市级联框\')', '1', '添加数据为太太太太成功');
INSERT INTO 57sy_common_log_201404 VALUES ('44', 'category_data/add', 'wangjian', '2014-04-23 15:31:12', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'太太太太-1\', 1, 1, 0, 1, \'城市级联框\')', '1', '添加数据为太太太太-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('45', 'category_data/add', 'wangjian', '2014-04-23 15:31:30', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'太太太太-1-1\', 1, 3, 0, 1, \'城市级联框\')', '1', '添加数据为太太太太-1-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('46', 'category_data/add', 'wangjian', '2014-04-23 15:32:25', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'太太太太-1\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为太太太太-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('47', 'category_data/add', 'wangjian', '2014-04-23 15:33:07', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'太太太太-1-1-1\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为太太太太-1-1-1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('48', 'category_data/add', 'wangjian', '2014-04-23 15:34:10', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'7777\', 1, 0, 14, 1, \'城市级联框\')', '1', '添加数据为7777成功');
INSERT INTO 57sy_common_log_201404 VALUES ('49', 'category_data/add', 'wangjian', '2014-04-23 15:34:30', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'7777\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为7777成功');
INSERT INTO 57sy_common_log_201404 VALUES ('50', 'category_data/add', 'wangjian', '2014-04-23 15:34:35', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'88888\', 1, 0, 16, 1, \'城市级联框\')', '1', '添加数据为88888成功');
INSERT INTO 57sy_common_log_201404 VALUES ('51', 'category_data/add', 'wangjian', '2014-04-23 15:34:39', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'77777\', 1, 0, 17, 1, \'城市级联框\')', '1', '添加数据为77777成功');
INSERT INTO 57sy_common_log_201404 VALUES ('52', 'category_data/add', 'wangjian', '2014-04-23 15:36:45', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'99999\', 0, 0, 18, 1, \'城市级联框\')', '1', '添加数据为99999成功');
INSERT INTO 57sy_common_log_201404 VALUES ('53', 'category_data/add', 'wangjian', '2014-04-23 15:36:57', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'发给各个\', 1, 0, 19, 1, \'城市级联框\')', '1', '添加数据为发给各个成功');
INSERT INTO 57sy_common_log_201404 VALUES ('54', 'category_data/add', 'wangjian', '2014-04-23 15:37:03', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'同一天\', 1, 0, 16, 1, \'城市级联框\')', '1', '添加数据为同一天成功');
INSERT INTO 57sy_common_log_201404 VALUES ('55', 'category_data/add', 'wangjian', '2014-04-23 15:37:11', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'56665\', 1, 0, 21, 1, \'城市级联框\')', '1', '添加数据为56665成功');
INSERT INTO 57sy_common_log_201404 VALUES ('56', 'category_data/add', 'wangjian', '2014-04-23 15:37:38', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'122121\', 1, 0, 22, 1, \'城市级联框\')', '1', '添加数据为122121成功');
INSERT INTO 57sy_common_log_201404 VALUES ('57', 'category_data/add', 'wangjian', '2014-04-23 15:37:43', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'23耳朵\', 1, 0, 23, 1, \'城市级联框\')', '1', '添加数据为23耳朵成功');
INSERT INTO 57sy_common_log_201404 VALUES ('58', 'category_data/add', 'wangjian', '2014-04-23 15:37:48', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'11111\', 1, 0, 24, 1, \'城市级联框\')', '1', '添加数据为11111成功');
INSERT INTO 57sy_common_log_201404 VALUES ('59', 'category_data/add', 'wangjian', '2014-04-23 15:37:57', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'vvvv\', 1, 0, 25, 1, \'城市级联框\')', '1', '添加数据为vvvv成功');
INSERT INTO 57sy_common_log_201404 VALUES ('60', 'category_data/add', 'wangjian', '2014-04-23 15:45:52', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'66777\', 1, 0, 21, 1, \'城市级联框\')', '1', '添加数据为66777成功');
INSERT INTO 57sy_common_log_201404 VALUES ('61', 'category_data/edit', 'wangjian', '2014-04-23 15:56:04', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'66777外网\'  where id = \'0\'', '0', '修改名称为66777外网失败');
INSERT INTO 57sy_common_log_201404 VALUES ('62', 'category_data/edit', 'wangjian', '2014-04-23 15:58:54', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'11111外网\'  where id = \'25\'', '1', '修改名称为11111外网成功');
INSERT INTO 57sy_common_log_201404 VALUES ('63', 'category_data/edit', 'wangjian', '2014-04-23 15:59:46', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'11111外网\'  where id = \'25\'', '1', '修改名称为11111外网成功');
INSERT INTO 57sy_common_log_201404 VALUES ('64', 'category_data/edit', 'wangjian', '2014-04-23 16:00:34', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'11111外网\',disorder = \'3\'  where id = \'25\'', '1', '修改名称为11111外网成功');
INSERT INTO 57sy_common_log_201404 VALUES ('65', 'category_data/add', 'wangjian', '2014-04-23 16:11:23', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'我的海淀\', 1, 2, 1, 1, \'城市级联框\')', '1', '添加数据为我的海淀成功');
INSERT INTO 57sy_common_log_201404 VALUES ('66', 'category_data/add', 'wangjian', '2014-04-23 16:13:00', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'周口市\', 1, 3, 3, 1, \'城市级联框\')', '1', '添加数据为周口市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('67', 'category_data/add', 'wangjian', '2014-04-23 16:14:47', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'999\', 1, 0, 0, 4, \'5566\')', '1', '添加数据为999成功');
INSERT INTO 57sy_common_log_201404 VALUES ('68', 'category_data/add', 'wangjian', '2014-04-23 16:14:55', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'llll\', 1, 0, 0, 4, \'5566\')', '1', '添加数据为llll成功');
INSERT INTO 57sy_common_log_201404 VALUES ('69', 'category_data/add', 'wangjian', '2014-04-23 16:15:01', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'121212\', 1, 0, 0, 4, \'5566\')', '1', '添加数据为121212成功');
INSERT INTO 57sy_common_log_201404 VALUES ('70', 'category_data/add', 'wangjian', '2014-04-23 16:40:37', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'周口子类\', 1, 0, 29, 1, \'城市级联框\')', '1', '添加数据为周口子类成功');
INSERT INTO 57sy_common_log_201404 VALUES ('71', 'category_data/add', 'wangjian', '2014-04-23 16:48:43', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'433443\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为433443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('72', 'category_data/add', 'wangjian', '2014-04-23 16:48:47', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'风尚大典\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为风尚大典成功');
INSERT INTO 57sy_common_log_201404 VALUES ('73', 'category_data/add', 'wangjian', '2014-04-23 16:48:50', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'vcv\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为vcv成功');
INSERT INTO 57sy_common_log_201404 VALUES ('74', 'category_data/add', 'wangjian', '2014-04-23 16:48:53', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'如同让他\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为如同让他成功');
INSERT INTO 57sy_common_log_201404 VALUES ('75', 'category_data/add', 'wangjian', '2014-04-23 16:48:57', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'形成vvc\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为形成vvc成功');
INSERT INTO 57sy_common_log_201404 VALUES ('76', 'category_data/add', 'wangjian', '2014-04-23 16:49:00', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'5655\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为5655成功');
INSERT INTO 57sy_common_log_201404 VALUES ('77', 'category_data/add', 'wangjian', '2014-04-23 16:49:03', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'犹太人由于\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为犹太人由于成功');
INSERT INTO 57sy_common_log_201404 VALUES ('78', 'category_data/add', 'wangjian', '2014-04-23 16:49:07', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'回个话\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为回个话成功');
INSERT INTO 57sy_common_log_201404 VALUES ('79', 'category_data/add', 'wangjian', '2014-04-23 16:49:11', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'能不能\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为能不能成功');
INSERT INTO 57sy_common_log_201404 VALUES ('80', 'category_data/add', 'wangjian', '2014-04-23 16:49:17', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'和法国恢复\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为和法国恢复成功');
INSERT INTO 57sy_common_log_201404 VALUES ('81', 'category_data/add', 'wangjian', '2014-04-23 16:49:21', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'变成vbcv\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为变成vbcv成功');
INSERT INTO 57sy_common_log_201404 VALUES ('82', 'category_data/add', 'wangjian', '2014-04-23 16:49:35', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'与语言\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为与语言成功');
INSERT INTO 57sy_common_log_201404 VALUES ('83', 'category_data/add', 'wangjian', '2014-04-23 16:49:39', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'好几个机会\', 1, 0, 0, 1, \'城市级联框\')', '1', '添加数据为好几个机会成功');
INSERT INTO 57sy_common_log_201404 VALUES ('84', 'login/dologin', 'wangjian', '2014-04-24 14:16:18', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('85', 'login/dologin', 'wangjian', '2014-04-24 14:19:51', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('86', 'category_data/edit', 'wangjian', '2014-04-24 14:45:20', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'5655\',disorder = \'0\'  where id = \'39\'', '0', '修改名称为5655失败');
INSERT INTO 57sy_common_log_201404 VALUES ('87', 'category_data/edit', 'wangjian', '2014-04-24 15:11:55', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'北京市5\',disorder = \'0\'  where id = \'1\'', '1', '修改名称为北京市5成功');
INSERT INTO 57sy_common_log_201404 VALUES ('88', 'category_data/edit', 'wangjian', '2014-04-24 15:12:13', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'北京市5\',disorder = \'0\'  where id = \'1\'', '0', '修改名称为北京市5失败');
INSERT INTO 57sy_common_log_201404 VALUES ('89', 'category_data/edit', 'wangjian', '2014-04-24 15:16:42', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'vcv\',disorder = \'0\'  where id = \'36\'', '0', '修改名称为vcv失败');
INSERT INTO 57sy_common_log_201404 VALUES ('90', 'category_data/edit', 'wangjian', '2014-04-24 15:17:20', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'和法国恢复1\',disorder = \'2\'  where id = \'43\'', '1', '修改名称为和法国恢复1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('91', 'category_data/edit', 'wangjian', '2014-04-24 15:26:53', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'好几个机会5\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为好几个机会5成功');
INSERT INTO 57sy_common_log_201404 VALUES ('92', 'category_data/edit', 'wangjian', '2014-04-24 15:28:38', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'好几个机会59\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为好几个机会59成功');
INSERT INTO 57sy_common_log_201404 VALUES ('93', 'category_data/edit', 'wangjian', '2014-04-24 15:31:46', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'好几个机会598\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为好几个机会598成功');
INSERT INTO 57sy_common_log_201404 VALUES ('94', 'category_data/edit', 'wangjian', '2014-04-24 15:32:09', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'895623\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为895623成功');
INSERT INTO 57sy_common_log_201404 VALUES ('95', 'category_data/edit', 'wangjian', '2014-04-24 15:34:21', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'895623与\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为895623与成功');
INSERT INTO 57sy_common_log_201404 VALUES ('96', 'category_data/edit', 'wangjian', '2014-04-24 15:34:40', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'34433443\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为34433443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('97', 'category_data/edit', 'wangjian', '2014-04-24 15:34:45', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'34433443\',disorder = \'4\'  where id = \'46\'', '1', '修改名称为34433443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('98', 'category_data/edit', 'wangjian', '2014-04-24 15:35:08', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'34433443\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为34433443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('99', 'category_data/edit', 'wangjian', '2014-04-24 15:35:27', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'南阳市1\',disorder = \'3\'  where id = \'4\'', '1', '修改名称为南阳市1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('100', 'category_data/edit', 'wangjian', '2014-04-24 15:35:48', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'南阳市\',disorder = \'3\'  where id = \'4\'', '1', '修改名称为南阳市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('101', 'category_data/edit', 'wangjian', '2014-04-24 15:36:40', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'周口子类\',disorder = \'6\'  where id = \'33\'', '1', '修改名称为周口子类成功');
INSERT INTO 57sy_common_log_201404 VALUES ('102', 'category_data/edit', 'wangjian', '2014-04-24 15:36:57', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'形成vv9\',disorder = \'0\'  where id = \'38\'', '1', '修改名称为形成vv9成功');
INSERT INTO 57sy_common_log_201404 VALUES ('103', 'category_data/edit', 'wangjian', '2014-04-24 15:37:36', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'同一天2\',disorder = \'0\'  where id = \'21\'', '1', '修改名称为同一天2成功');
INSERT INTO 57sy_common_log_201404 VALUES ('104', 'category_data/edit', 'wangjian', '2014-04-24 15:39:41', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'77777\',disorder = \'0\'  where id = \'18\'', '0', '修改名称为77777失败');
INSERT INTO 57sy_common_log_201404 VALUES ('105', 'category_data/edit', 'wangjian', '2014-04-24 15:43:15', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'香花镇\',disorder = \'5\'  where id = \'6\'', '1', '修改名称为香花镇成功');
INSERT INTO 57sy_common_log_201404 VALUES ('106', 'category_data/edit', 'wangjian', '2014-04-24 15:44:10', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'香花镇\',disorder = \'6\'  where id = \'6\'', '1', '修改名称为香花镇成功');
INSERT INTO 57sy_common_log_201404 VALUES ('107', 'category_data/edit', 'wangjian', '2014-04-24 15:45:40', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀\',disorder = \'2\'  where id = \'28\'', '0', '修改名称为我的海淀失败');
INSERT INTO 57sy_common_log_201404 VALUES ('108', 'category_data/edit', 'wangjian', '2014-04-24 15:45:52', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀\',disorder = \'2\'  where id = \'28\'', '0', '修改名称为我的海淀失败');
INSERT INTO 57sy_common_log_201404 VALUES ('109', 'category_data/edit', 'wangjian', '2014-04-24 15:47:20', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀\',disorder = \'2\'  where id = \'28\'', '0', '修改名称为我的海淀失败');
INSERT INTO 57sy_common_log_201404 VALUES ('110', 'category_data/edit', 'wangjian', '2014-04-24 15:48:08', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀\',disorder = \'2\'  where id = \'28\'', '0', '修改名称为我的海淀失败');
INSERT INTO 57sy_common_log_201404 VALUES ('111', 'category_data/edit', 'wangjian', '2014-04-24 15:48:32', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀\',disorder = \'2\'  where id = \'28\'', '0', '修改名称为我的海淀失败');
INSERT INTO 57sy_common_log_201404 VALUES ('112', 'category_data/edit', 'wangjian', '2014-04-24 15:48:35', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我的海淀3\',disorder = \'2\'  where id = \'28\'', '1', '修改名称为我的海淀3成功');
INSERT INTO 57sy_common_log_201404 VALUES ('113', 'category_data/edit', 'wangjian', '2014-04-24 15:48:43', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'海淀13\',disorder = \'1\'  where id = \'7\'', '1', '修改名称为海淀13成功');
INSERT INTO 57sy_common_log_201404 VALUES ('114', 'category_data/edit', 'wangjian', '2014-04-24 15:48:58', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'香花镇1\',disorder = \'6\'  where id = \'6\'', '1', '修改名称为香花镇1成功');
INSERT INTO 57sy_common_log_201404 VALUES ('115', 'nav/add', 'wangjian', '2014-04-24 15:50:37', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (78, \'添加模型数据\', 1, \'2014-04-24 15:50:37\', \'category_data/add/\', 1)', '1', '添加导航添加模型数据成功');
INSERT INTO 57sy_common_log_201404 VALUES ('116', 'nav/add', 'wangjian', '2014-04-24 15:51:02', '127.0.0.1', 'INSERT INTO `57sy_common_admin_nav` (`pid`, `name`, `status`, `addtime`, `url`, `disorder`) VALUES (78, \'编辑联动数据\', 1, \'2014-04-24 15:51:02\', \'category_data/edit/\', 3)', '1', '添加导航编辑联动数据成功');
INSERT INTO 57sy_common_log_201404 VALUES ('117', 'role/edit', 'wangjian', '2014-04-24 15:51:17', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'系统管理\' , perm = \'a:7:{i:0;s:11:\"main/index/\";i:1;s:16:\"sysconfig/index/\";i:2;s:14:\"sysconfig/add/\";i:3;s:15:\"category/index/\";i:4;s:13:\"category/add/\";i:5;s:20:\"category_data/index/\";i:6;s:18:\"category_data/add/\";}\',', '1', '修改角色为系统管理成功');
INSERT INTO 57sy_common_log_201404 VALUES ('118', 'login/dologin', 'taotao', '2014-04-24 15:51:46', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('119', 'login/dologin', 'taotao', '2014-04-24 15:54:55', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('120', 'login/dologin', 'wangjian', '2014-04-24 15:56:07', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('121', 'sys_admin/edit', 'wangjian', '2014-04-24 15:56:29', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:2:{i:0;s:15:\"sysconfig/edit/\";i:1;s:19:\"category_data/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201404 VALUES ('122', 'category_data/edit', 'taotao', '2014-04-24 16:01:34', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'河南省\',disorder = \'3\'  where id = \'3\'', '0', '修改名称为河南省失败');
INSERT INTO 57sy_common_log_201404 VALUES ('123', 'category_data/edit', 'taotao', '2014-04-24 16:01:41', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'如同让他\',disorder = \'5\'  where id = \'37\'', '1', '修改名称为如同让他成功');
INSERT INTO 57sy_common_log_201404 VALUES ('124', 'category_data/edit', 'taotao', '2014-04-24 16:01:47', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'如同让他\',disorder = \'5\'  where id = \'37\'', '1', '修改名称为如同让他成功');
INSERT INTO 57sy_common_log_201404 VALUES ('125', 'category_data/edit', 'taotao', '2014-04-24 16:01:59', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'0\',name = \'6666\',disorder = \'0\'  where id = \'46\'', '1', '修改名称为6666成功');
INSERT INTO 57sy_common_log_201404 VALUES ('126', 'login/dologin', 'wangjian', '2014-04-25 09:17:59', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('127', 'login/dologin', 'taotao', '2014-04-25 09:18:50', '127.0.0.1', 'login_sql', '1', '用户taotao登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('128', 'login/dologin', 'wangjian', '2014-04-25 09:19:28', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('129', 'sys_admin/edit', 'wangjian', '2014-04-25 09:19:52', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'1\' , gid = \'1\', perm = \'a:1:{i:0;s:15:\"sysconfig/edit/\";}\' ,username = \'taotao\',super_admin = \'0\' where id = \'3\'', '1', '修改用户为taotao成功');
INSERT INTO 57sy_common_log_201404 VALUES ('130', 'login/dologin', 'wangjian', '2014-04-25 09:28:51', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('131', 'category_data/add', 'wangjian', '2014-04-25 09:45:04', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'77777\', 1, 0, 1, 1, \'城市级联框\')', '1', '添加数据为77777成功');
INSERT INTO 57sy_common_log_201404 VALUES ('132', 'category_data/add', 'wangjian', '2014-04-25 09:45:23', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'78u8\', 0, 9, 45, 1, \'城市级联框\')', '1', '添加数据为78u8成功');
INSERT INTO 57sy_common_log_201404 VALUES ('133', 'category_data/add', 'wangjian', '2014-04-25 09:47:25', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'1234\', 1, 0, 35, 1, \'城市级联框\')', '1', '添加数据为1234成功');
INSERT INTO 57sy_common_log_201404 VALUES ('134', 'category_data/add', 'wangjian', '2014-04-25 09:49:50', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'77565\', 1, 0, 0, 5, \'dasd\')', '1', '添加数据为77565成功');
INSERT INTO 57sy_common_log_201404 VALUES ('135', 'category_data/add', 'wangjian', '2014-04-25 09:50:02', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'我来测试顶级分类\', 1, 0, 0, 5, \'dasd\')', '1', '添加数据为我来测试顶级分类成功');
INSERT INTO 57sy_common_log_201404 VALUES ('136', 'category_data/edit', 'wangjian', '2014-04-25 09:50:08', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'我来测试顶级分类\',disorder = \'4\'  where id = \'51\'', '1', '修改名称为我来测试顶级分类成功');
INSERT INTO 57sy_common_log_201404 VALUES ('137', 'category_data/add', 'wangjian', '2014-04-25 09:51:02', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'噼噼啪啪\', 1, 3, 0, 5, \'dasd\')', '1', '添加数据为噼噼啪啪成功');
INSERT INTO 57sy_common_log_201404 VALUES ('138', 'category_data/edit', 'wangjian', '2014-04-25 10:17:21', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'23434\',disorder = \'0\'  where id = \'50\'', '1', '修改名称为23434成功');
INSERT INTO 57sy_common_log_201404 VALUES ('139', 'category_data/edit', 'wangjian', '2014-04-25 10:17:42', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'2343454\',disorder = \'0\'  where id = \'50\'', '1', '修改名称为2343454成功');
INSERT INTO 57sy_common_log_201404 VALUES ('140', 'category_data/edit', 'wangjian', '2014-04-25 10:18:23', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪\',disorder = \'3\'  where id = \'52\'', '0', '修改名称为噼噼啪啪失败');
INSERT INTO 57sy_common_log_201404 VALUES ('141', 'category_data/edit', 'wangjian', '2014-04-25 10:19:38', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪\',disorder = \'3\'  where id = \'52\'', '0', '修改名称为噼噼啪啪失败');
INSERT INTO 57sy_common_log_201404 VALUES ('142', 'category_data/edit', 'wangjian', '2014-04-25 10:19:50', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪\',disorder = \'3\'  where id = \'52\'', '0', '修改名称为噼噼啪啪失败');
INSERT INTO 57sy_common_log_201404 VALUES ('143', 'category_data/edit', 'wangjian', '2014-04-25 10:20:07', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪\',disorder = \'3\'  where id = \'52\'', '0', '修改名称为噼噼啪啪失败');
INSERT INTO 57sy_common_log_201404 VALUES ('144', 'category_data/edit', 'wangjian', '2014-04-25 10:20:49', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪\',disorder = \'3\'  where id = \'52\'', '0', '修改名称为噼噼啪啪失败');
INSERT INTO 57sy_common_log_201404 VALUES ('145', 'category_data/edit', 'wangjian', '2014-04-25 10:21:00', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼啪啪5\',disorder = \'3\'  where id = \'52\'', '1', '修改名称为噼噼啪啪5成功');
INSERT INTO 57sy_common_log_201404 VALUES ('146', 'category_data/edit', 'wangjian', '2014-04-25 10:21:17', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'噼噼54\',disorder = \'3\'  where id = \'52\'', '1', '修改名称为噼噼54成功');
INSERT INTO 57sy_common_log_201404 VALUES ('147', 'category_data/edit', 'wangjian', '2014-04-25 10:23:49', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'667\',disorder = \'3\'  where id = \'52\'', '1', '修改名称为667成功');
INSERT INTO 57sy_common_log_201404 VALUES ('148', 'category_data/edit', 'wangjian', '2014-04-25 10:24:06', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'66756\',disorder = \'3\'  where id = \'52\'', '1', '修改名称为66756成功');
INSERT INTO 57sy_common_log_201404 VALUES ('149', 'category_data/add', 'wangjian', '2014-04-25 10:24:29', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'assault\', 1, 3, 52, 5, \'dasd\')', '1', '添加数据为assault成功');
INSERT INTO 57sy_common_log_201404 VALUES ('150', 'category_data/add', 'wangjian', '2014-04-25 10:25:31', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'32321\', 1, 323, 53, 5, \'dasd\')', '1', '添加数据为32321成功');
INSERT INTO 57sy_common_log_201404 VALUES ('151', 'category_data/add', 'wangjian', '2014-04-25 10:26:58', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'测试123\', 1, 0, 0, 5, \'dasd\')', '1', '添加数据为测试123成功');
INSERT INTO 57sy_common_log_201404 VALUES ('152', 'category_data/add', 'wangjian', '2014-04-25 10:27:19', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'12233\', 1, 1, 55, 5, \'dasd\')', '1', '添加数据为12233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('153', 'category_data/add', 'wangjian', '2014-04-25 10:31:17', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'武器武器\', 1, 0, 0, 5, \'dasd\')', '1', '添加数据为武器武器成功');
INSERT INTO 57sy_common_log_201404 VALUES ('154', 'category/edit', 'wangjian', '2014-04-25 10:33:21', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'城市级联框\',modifytime = \'2014-04-25 10:33:21\',remark = \'\'   where id = \'1\'', '1', '修改名称为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('155', 'category/add', 'wangjian', '2014-04-25 10:35:07', '127.0.0.1', 'INSERT INTO `57sy_common_category_type` (`cname`, `status`, `addtime`, `addperson`, `modifytime`, `remark`) VALUES (\'城市级联框\', 1, \'2014-04-25 10:35:07\', \'wangjian\', \'2014-04-25 10:35:07\', \'城市级联框\')', '1', '添加模型为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('156', 'category_data/add', 'wangjian', '2014-04-25 10:35:24', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'北京市\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为北京市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('157', 'category_data/add', 'wangjian', '2014-04-25 10:35:45', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'河南省\', 1, 1, 0, 6, \'城市级联框\')', '1', '添加数据为河南省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('158', 'category_data/add', 'wangjian', '2014-04-25 10:35:53', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'上海市\', 1, 2, 0, 6, \'城市级联框\')', '1', '添加数据为上海市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('159', 'category_data/add', 'wangjian', '2014-04-25 10:36:11', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'河北省\', 1, 4, 0, 6, \'城市级联框\')', '1', '添加数据为河北省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('160', 'category_data/add', 'wangjian', '2014-04-25 10:36:27', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'湖南省\', 1, 6, 0, 6, \'城市级联框\')', '1', '添加数据为湖南省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('161', 'category_data/add', 'wangjian', '2014-04-25 10:37:10', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'东城区\', 1, 0, 58, 6, \'城市级联框\')', '1', '添加数据为东城区成功');
INSERT INTO 57sy_common_log_201404 VALUES ('162', 'category_data/add', 'wangjian', '2014-04-25 10:37:23', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'西城区\', 1, 0, 58, 6, \'城市级联框\')', '1', '添加数据为西城区成功');
INSERT INTO 57sy_common_log_201404 VALUES ('163', 'category_data/add', 'wangjian', '2014-04-25 10:37:41', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'朝阳区\', 1, 0, 58, 6, \'城市级联框\')', '1', '添加数据为朝阳区成功');
INSERT INTO 57sy_common_log_201404 VALUES ('164', 'category_data/add', 'wangjian', '2014-04-25 10:37:49', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'海淀区\', 1, 0, 58, 6, \'城市级联框\')', '1', '添加数据为海淀区成功');
INSERT INTO 57sy_common_log_201404 VALUES ('165', 'category_data/add', 'wangjian', '2014-04-25 10:37:58', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'南阳市\', 1, 0, 59, 6, \'城市级联框\')', '1', '添加数据为南阳市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('166', 'category_data/add', 'wangjian', '2014-04-25 10:38:20', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'洛阳市\', 1, 0, 59, 6, \'城市级联框\')', '1', '添加数据为洛阳市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('167', 'category_data/add', 'wangjian', '2014-04-25 10:38:29', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'周口市\', 1, 0, 59, 6, \'城市级联框\')', '1', '添加数据为周口市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('168', 'category_data/add', 'wangjian', '2014-04-25 10:41:03', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'淅川县\', 1, 0, 67, 6, \'城市级联框\')', '1', '添加数据为淅川县成功');
INSERT INTO 57sy_common_log_201404 VALUES ('169', 'category_data/add', 'wangjian', '2014-04-25 10:41:50', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'邓州市\', 1, 0, 67, 6, \'城市级联框\')', '1', '添加数据为邓州市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('170', 'category_data/add', 'wangjian', '2014-04-25 10:43:32', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'宝山\', 1, 0, 60, 6, \'城市级联框\')', '1', '添加数据为宝山成功');
INSERT INTO 57sy_common_log_201404 VALUES ('171', 'category/edit', 'wangjian', '2014-04-25 10:44:02', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'0\',cname = \'城市级联框\',modifytime = \'2014-04-25 10:44:02\',remark = \'城市级联框\'   where id = \'6\'', '1', '修改名称为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('172', 'category/edit', 'wangjian', '2014-04-25 10:44:10', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'城市级联框\',modifytime = \'2014-04-25 10:44:10\',remark = \'城市级联框\'   where id = \'6\'', '1', '修改名称为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('173', 'category_data/add', 'wangjian', '2014-04-25 10:44:29', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'广东省\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为广东省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('174', 'category_data/add', 'wangjian', '2014-04-25 10:44:40', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'湖北省\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为湖北省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('175', 'category_data/add', 'wangjian', '2014-04-25 10:44:50', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'江苏省\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为江苏省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('176', 'category_data/add', 'wangjian', '2014-04-25 10:45:05', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'四川省\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为四川省成功');
INSERT INTO 57sy_common_log_201404 VALUES ('177', 'login/dologin', 'wangjian', '2014-04-26 21:05:12', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('178', 'login/dologin', 'wangjian', '2014-04-26 21:37:20', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('179', 'login/dologin', 'wangjian', '2014-04-26 22:22:03', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('180', 'login/dologin', 'wangjian', '2014-04-26 22:22:50', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('181', 'category_data/add', 'wangjian', '2014-04-26 22:24:40', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'43443\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为43443成功');
INSERT INTO 57sy_common_log_201404 VALUES ('182', 'category_data/add', 'wangjian', '2014-04-26 22:24:51', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'4343\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为4343成功');
INSERT INTO 57sy_common_log_201404 VALUES ('183', 'category_data/add', 'wangjian', '2014-04-26 22:25:14', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'23332\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为23332成功');
INSERT INTO 57sy_common_log_201404 VALUES ('184', 'category_data/add', 'wangjian', '2014-04-26 22:25:19', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'sdasd\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为sdasd成功');
INSERT INTO 57sy_common_log_201404 VALUES ('185', 'category_data/add', 'wangjian', '2014-04-26 22:25:38', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'4434\', 1, 0, 76, 6, \'城市级联框\')', '1', '添加数据为4434成功');
INSERT INTO 57sy_common_log_201404 VALUES ('186', 'category_data/add', 'wangjian', '2014-04-26 22:25:47', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'kjkjjkj\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为kjkjjkj成功');
INSERT INTO 57sy_common_log_201404 VALUES ('187', 'category_data/add', 'wangjian', '2014-04-26 22:31:44', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'sdfsfsd\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为sdfsfsd成功');
INSERT INTO 57sy_common_log_201404 VALUES ('188', 'category_data/add', 'wangjian', '2014-04-26 22:31:49', '127.0.0.1', 'INSERT INTO `57sy_common_category_data` (`name`, `status`, `disorder`, `pid`, `typeid`, `typename`) VALUES (\'sfdsf\', 1, 0, 0, 6, \'城市级联框\')', '1', '添加数据为sfdsf成功');
INSERT INTO 57sy_common_log_201404 VALUES ('189', 'login/dologin', 'wangjian', '2014-04-27 12:19:46', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('190', 'sysconfig/edit', 'wangjian', '2014-04-27 12:20:00', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('191', 'sysconfig/edit', 'wangjian', '2014-04-27 12:21:07', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('192', 'category/edit', 'wangjian', '2014-04-27 12:23:09', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'城市级联框\',modifytime = \'2014-04-27 12:23:09\',remark = \'城市级联框描述信息\'   where id = \'6\'', '1', '修改名称为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('193', 'category/edit', 'wangjian', '2014-04-27 12:25:58', '127.0.0.1', 'UPDATE `57sy_common_category_type` SET status = \'1\',cname = \'城市级联框\',modifytime = \'2014-04-27 12:25:58\',remark = \'城市级联框描述信息\'   where id = \'6\'', '1', '修改名称为城市级联框成功');
INSERT INTO 57sy_common_log_201404 VALUES ('194', 'sysconfig/edit', 'wangjian', '2014-04-27 12:40:08', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('195', 'sysconfig/edit', 'wangjian', '2014-04-27 12:40:32', '127.0.0.1', 'sysconfig_update', '1', '修改系统变量成功');
INSERT INTO 57sy_common_log_201404 VALUES ('196', 'role/edit', 'wangjian', '2014-04-27 12:44:50', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:3:{i:0;s:13:\"sys_admin/add\";i:1;s:19:\"category_data/edit/\";i:2;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为1233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('197', 'role/edit', 'wangjian', '2014-04-27 12:47:50', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:3:{i:0;s:13:\"sys_admin/add\";i:1;s:19:\"category_data/edit/\";i:2;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '0', '修改角色为1233失败');
INSERT INTO 57sy_common_log_201404 VALUES ('198', 'role/edit', 'wangjian', '2014-04-27 12:48:25', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:4:{i:0;s:11:\"role/index/\";i:1;s:13:\"sys_admin/add\";i:2;s:19:\"category_data/edit/\";i:3;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为1233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('199', 'role/edit', 'wangjian', '2014-04-27 12:48:52', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:5:{i:0;s:16:\"sysconfig/index/\";i:1;s:11:\"role/index/\";i:2;s:13:\"sys_admin/add\";i:3;s:19:\"category_data/edit/\";i:4;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为1233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('200', 'role/edit', 'wangjian', '2014-04-27 12:49:54', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:6:{i:0;s:16:\"sysconfig/index/\";i:1;s:15:\"sysconfig/edit/\";i:2;s:11:\"role/index/\";i:3;s:13:\"sys_admin/add\";i:4;s:19:\"category_data/edit/\";i:5;s:3:\"555\";}\',status = \'0\' where id = \'6\'', '1', '修改角色为1233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('201', 'role/edit', 'wangjian', '2014-04-27 12:50:35', '127.0.0.1', 'UPDATE 57sy_common_role SET rolename = \'1233\' , perm = \'a:7:{i:0;s:16:\"sysconfig/index/\";i:1;s:15:\"sysconfig/edit/\";i:2;s:11:\"role/index/\";i:3;s:13:\"sys_admin/add\";i:4;s:14:\"sys_admin/edit\";i:5;s:19:\"category_data/edit/\";i:6;s:3:\"555\";}\',status = \'0\' wher', '1', '修改角色为1233成功');
INSERT INTO 57sy_common_log_201404 VALUES ('202', 'category_data/edit', 'wangjian', '2014-04-27 12:51:21', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'北京市\',disorder = \'3\'  where id = \'58\'', '1', '修改名称为北京市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('203', 'category_data/edit', 'wangjian', '2014-04-27 12:52:09', '127.0.0.1', 'UPDATE `57sy_common_category_data` SET status = \'1\',name = \'北京市\',disorder = \'6\'  where id = \'58\'', '1', '修改名称为北京市成功');
INSERT INTO 57sy_common_log_201404 VALUES ('204', 'login/dologin', 'wangjian', '2014-04-27 15:08:08', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201404 VALUES ('205', 'sys_admin/edit', 'wangjian', '2014-04-27 16:16:04', '127.0.0.1', 'UPDATE `57sy_common_system_user` SET status = \'0\' , gid = \'2\', perm = \'a:1:{i:0;s:11:\"role/index/\";}\' ,username = \'5567777\',super_admin = \'0\' where id = \'13\'', '1', '修改用户为5567777成功');

-- ----------------------------
-- Table structure for `57sy_common_log_201405`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_log_201405`;
CREATE TABLE `57sy_common_log_201405` (
  `log_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `log_url` varchar(50) NOT NULL,
  `log_person` varchar(16) NOT NULL,
  `log_time` datetime NOT NULL,
  `log_ip` char(15) NOT NULL,
  `log_sql` text NOT NULL,
  `log_status` tinyint(1) NOT NULL DEFAULT '1',
  `log_message` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_log_201405
-- ----------------------------
INSERT INTO 57sy_common_log_201405 VALUES ('1', 'login/dologin', 'wangjian', '2014-05-01 20:26:47', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('2', 'login/dologin', 'wangjian', '2014-05-01 20:27:44', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('3', 'login/dologin', 'wangjian', '2014-05-01 20:30:11', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('4', 'login/dologin', 'wangjian', '2014-05-02 02:20:14', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('5', 'nav/edit', 'wangjian', '2014-05-02 02:38:41', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'网站基本信息\',url = \'\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'67\'', '0', '编辑导航网站基本信息失败');
INSERT INTO 57sy_common_log_201405 VALUES ('6', 'nav/edit', 'wangjian', '2014-05-02 02:38:58', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'网站基本信息\',url = \'\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'1\' where id = \'67\'', '1', '编辑导航网站基本信息成功');
INSERT INTO 57sy_common_log_201405 VALUES ('7', 'nav/edit', 'wangjian', '2014-05-02 02:40:50', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'网站基本信息\',url = \'\' ,disorder = \'1\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'67\'', '1', '编辑导航网站基本信息成功');
INSERT INTO 57sy_common_log_201405 VALUES ('8', 'nav/edit', 'wangjian', '2014-05-02 02:41:05', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'21\',name = \'后台团队\',url = \'\' ,disorder = \'3\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'1\' where id = \'66\'', '1', '编辑导航后台团队成功');
INSERT INTO 57sy_common_log_201405 VALUES ('9', 'login/dologin', 'wangjian', '2014-05-03 09:21:55', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('10', 'login/dologin', 'wangjian', '2014-05-03 09:22:37', '127.0.0.1', 'login_sql', '1', '用户wangjian登录成功');
INSERT INTO 57sy_common_log_201405 VALUES ('11', 'nav/edit', 'wangjian', '2014-05-03 09:53:03', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'22\',name = \'网站用户管理\',url = \'\' ,disorder = \'2\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'32\'', '1', '编辑导航网站用户管理成功');
INSERT INTO 57sy_common_log_201405 VALUES ('12', 'nav/edit', 'wangjian', '2014-05-03 09:53:16', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'32\',name = \'用户列表\',url = \'dd/list/\' ,disorder = \'5\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'33\'', '1', '编辑导航用户列表成功');
INSERT INTO 57sy_common_log_201405 VALUES ('13', 'nav/edit', 'wangjian', '2014-05-03 09:53:28', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'33\',name = \'用户添加\',url = \'6767677\' ,disorder = \'90\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'52\'', '1', '编辑导航用户添加成功');
INSERT INTO 57sy_common_log_201405 VALUES ('14', 'nav/edit', 'wangjian', '2014-05-03 09:53:36', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'33\',name = \'用户添加\',url = \'user/add/\' ,disorder = \'90\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'52\'', '1', '编辑导航用户添加成功');
INSERT INTO 57sy_common_log_201405 VALUES ('15', 'nav/edit', 'wangjian', '2014-05-03 09:53:54', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'32\',name = \'用户列表\',url = \'user/index/\' ,disorder = \'5\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'33\'', '1', '编辑导航用户列表成功');
INSERT INTO 57sy_common_log_201405 VALUES ('16', 'nav/edit', 'wangjian', '2014-05-03 09:54:15', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'33\',name = \'用户编辑\',url = \'555\' ,disorder = \'98\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'41\'', '1', '编辑导航用户编辑成功');
INSERT INTO 57sy_common_log_201405 VALUES ('17', 'nav/edit', 'wangjian', '2014-05-03 09:54:28', '127.0.0.1', 'UPDATE `57sy_common_admin_nav` set pid = \'33\',name = \'用户编辑\',url = \'user/edit/\' ,disorder = \'98\' ,status = \'1\' ,url_type = \'1\' ,`collapsed` = \'0\' where id = \'41\'', '1', '编辑导航用户编辑成功');
INSERT INTO 57sy_common_log_201405 VALUES ('18', 'user/add', 'wangjian', '2014-05-03 10:07:47', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`) VALUES (\'webuser\', 1, 1399082867, \'44bc64daf3c37a7ada23e63a137310bd\')', '1', '添加用户为webuser成功');
INSERT INTO 57sy_common_log_201405 VALUES ('19', 'user/add', 'wangjian', '2014-05-03 10:22:35', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'taotao\', 1, 1399083755, \'44bc64daf3c37a7ada23e63a137310bd\', 1399170120)', '1', '添加用户为taotao成功');
INSERT INTO 57sy_common_log_201405 VALUES ('20', 'user/add', 'wangjian', '2014-05-03 10:24:48', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'haohao\', 1, 1399083888, \'44bc64daf3c37a7ada23e63a137310bd\', 0)', '1', '添加用户为haohao成功');
INSERT INTO 57sy_common_log_201405 VALUES ('21', 'user/add', 'wangjian', '2014-05-03 10:25:13', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'aojiao\', 1, 1399083913, \'44bc64daf3c37a7ada23e63a137310bd\', 1399775100)', '1', '添加用户为aojiao成功');
INSERT INTO 57sy_common_log_201405 VALUES ('22', 'role/edit', 'wangjian', '2014-05-03 10:47:49', '127.0.0.1', 'no sql', '0', '请选择权限');
INSERT INTO 57sy_common_log_201405 VALUES ('23', 'user/edit', 'wangjian', '2014-05-03 10:50:20', '127.0.0.1', 'UPDATE `57sy_common_user` SET `expire` = \'1399775100\' ,`username` = \'aojiao\',`passwd`= \'d108ea09b955e95d68a9668a2b9c9443\' ,`status` = \'1\' where uid = \'4\'', '1', '修改用户为aojiao成功');
INSERT INTO 57sy_common_log_201405 VALUES ('24', 'user/edit', 'wangjian', '2014-05-03 10:50:30', '127.0.0.1', 'UPDATE `57sy_common_user` SET `expire` = \'1399861500\' ,`username` = \'aojiao\' ,`status` = \'1\' where uid = \'4\'', '1', '修改用户为aojiao成功');
INSERT INTO 57sy_common_log_201405 VALUES ('25', 'user/add', 'wangjian', '2014-05-03 10:51:00', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'1233\', 1, 1399085460, \'78bf4f00f81a36b57950e239f1df91c1\', 0)', '1', '添加用户为1233成功');
INSERT INTO 57sy_common_log_201405 VALUES ('26', 'user/add', 'wangjian', '2014-05-03 10:51:09', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'45666\', 1, 1399085469, \'b35b31a24acc2da3bd9e3feb30fc7e79\', 0)', '1', '添加用户为45666成功');
INSERT INTO 57sy_common_log_201405 VALUES ('27', 'user/add', 'wangjian', '2014-05-03 10:51:17', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'十分丰富\', 1, 1399085477, \'2aa11f3f451627b14158e812f5fd6ce7\', 0)', '1', '添加用户为十分丰富成功');
INSERT INTO 57sy_common_log_201405 VALUES ('28', 'user/add', 'wangjian', '2014-05-03 10:51:27', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'566yyy\', 1, 1399085486, \'8523109c9a85dbfc46eb1f46955b5449\', 0)', '1', '添加用户为566yyy成功');
INSERT INTO 57sy_common_log_201405 VALUES ('29', 'user/add', 'wangjian', '2014-05-03 10:54:18', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'5345\', 1, 1399085657, \'a2b45e7eaa7a1376c3fb1b13fd31620b\', 0)', '1', '添加用户为5345成功');
INSERT INTO 57sy_common_log_201405 VALUES ('30', 'user/add', 'wangjian', '2014-05-03 10:54:26', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'mytest\', 1, 1399085666, \'11c5587af2863955b7c04c59a8732ccc\', 0)', '1', '添加用户为mytest成功');
INSERT INTO 57sy_common_log_201405 VALUES ('31', 'user/add', 'wangjian', '2014-05-03 10:55:06', '127.0.0.1', 'INSERT INTO `57sy_common_user` (`username`, `status`, `regdate`, `passwd`, `expire`) VALUES (\'45rere\', 1, 1399085706, \'fdf4511ac15862ff29a861c389dede90\', 0)', '1', '添加用户为45rere成功');
INSERT INTO 57sy_common_log_201405 VALUES ('32', 'user/edit', 'wangjian', '2014-05-03 11:03:12', '127.0.0.1', 'UPDATE `57sy_common_user` SET `expire` = \'0\' ,`username` = \'45rere\' ,`status` = \'0\' where uid = \'12\'', '1', '修改用户为45rere成功');

-- ----------------------------
-- Table structure for `57sy_common_role`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_role`;
CREATE TABLE `57sy_common_role` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `rolename` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '添加日期',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `perm` text COMMENT '对应的权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_role
-- ----------------------------
INSERT INTO 57sy_common_role VALUES ('1', '系统管理', '2014-01-27 16:12:35', '1', 'a:7:{i:0;s:11:\"main/index/\";i:1;s:16:\"sysconfig/index/\";i:2;s:14:\"sysconfig/add/\";i:3;s:15:\"category/index/\";i:4;s:13:\"category/add/\";i:5;s:20:\"category_data/index/\";i:6;s:18:\"category_data/add/\";}');
INSERT INTO 57sy_common_role VALUES ('2', '编辑', '2014-01-26 16:14:27', '1', 'a:3:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";}');
INSERT INTO 57sy_common_role VALUES ('3', 'dasd', '2014-02-03 16:14:37', '1', 'a:4:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:14:\"sys_admin/add/\";}');
INSERT INTO 57sy_common_role VALUES ('4', '娃娃亲', '2014-02-13 16:14:43', '1', 'a:7:{i:0;s:10:\"nav/index/\";i:1;s:8:\"nav/add/\";i:2;s:9:\"nav/edit/\";i:3;s:16:\"sys_admin/index/\";i:4;s:8:\"dd/list/\";i:5;s:3:\"555\";i:6;s:7:\"6767677\";}');
INSERT INTO 57sy_common_role VALUES ('5', 'oooo', '2014-03-03 03:31:13', '1', '');
INSERT INTO 57sy_common_role VALUES ('6', '1233', '2014-03-03 03:31:30', '0', 'a:7:{i:0;s:16:\"sysconfig/index/\";i:1;s:15:\"sysconfig/edit/\";i:2;s:11:\"role/index/\";i:3;s:13:\"sys_admin/add\";i:4;s:14:\"sys_admin/edit\";i:5;s:19:\"category_data/edit/\";i:6;s:3:\"555\";}');

-- ----------------------------
-- Table structure for `57sy_common_sysconfig`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_sysconfig`;
CREATE TABLE `57sy_common_sysconfig` (
  `varname` varchar(20) DEFAULT NULL,
  `value` text,
  `info` varchar(100) DEFAULT NULL COMMENT '说明',
  `groupid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL DEFAULT 'string' COMMENT '变量类型',
  UNIQUE KEY `varname` (`varname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_sysconfig
-- ----------------------------
INSERT INTO 57sy_common_sysconfig VALUES ('cdf_install', 'install/3&#039;', 'DedeCMS安装目录', '1', 'string');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_cookie_encode', 'UsSQu3494J', 'cookie的加密密码\r\n', '1', 'string');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_tu', '125689', '缩略图默认宽度', '3', 'string');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_isopen_huiyuan', 'Y', '是否开启会员功能', '2', 'boolean');
INSERT INTO 57sy_common_sysconfig VALUES ('des', '我的站点', '站点描述\r\n', '1', 'textarea');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_zhian_open', 'N', '站点是否开启', '1', 'boolean');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_pin', '100', '么次访问的次数\r\n', '1', 'string');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_memcsche', 'N', '是否开启memcache缓存', '3', 'boolean');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg-xingneng', '120_', '性能值', '3', 'number');
INSERT INTO 57sy_common_sysconfig VALUES ('yyuuu', '1233', '4434343', '1', 'number');
INSERT INTO 57sy_common_sysconfig VALUES ('cfg_ssa_', '爱的', '我的撒旦', '1', 'string');

-- ----------------------------
-- Table structure for `57sy_common_system_user`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_system_user`;
CREATE TABLE `57sy_common_system_user` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(16) NOT NULL COMMENT '后台管理的用户名',
  `passwd` char(32) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `gid` smallint(8) unsigned NOT NULL DEFAULT '0' COMMENT '属于哪个群组',
  `addtime` datetime NOT NULL COMMENT '添加日期',
  `super_admin` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是超级管理员 1是有一切权限 0不是需要配置权限',
  `perm` varchar(255) DEFAULT NULL COMMENT '用户的其他权限',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_system_user
-- ----------------------------
INSERT INTO 57sy_common_system_user VALUES ('1', 'wangjian', '44bc64daf3c37a7ada23e63a137310bd', '1', '2', '0000-00-00 00:00:00', '1', '');
INSERT INTO 57sy_common_system_user VALUES ('2', 'ewew', 'ewwe', '0', '4', '0000-00-00 00:00:00', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('3', 'taotao', '44bc64daf3c37a7ada23e63a137310bd', '1', '1', '2014-03-01 02:07:15', '0', 'a:1:{i:0;s:15:\"sysconfig/edit/\";}');
INSERT INTO 57sy_common_system_user VALUES ('4', 'haohao91', 'wangjian', '1', '2', '2014-03-01 02:07:36', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('5', '陪陪12', 'wangjian', '1', '3', '2014-03-01 02:08:27', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('6', 'caiying', '44bc64daf3c37a7ada23e63a137310bd', '1', '3', '2014-03-01 15:23:30', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('7', 'lili', '98781d5569adb6555e15f5194c149483', '1', '1', '2014-03-01 15:25:50', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('8', '5555', '5b1b68a9abf4d2cd155c81a9225fd158', '1', '3', '2014-03-01 15:26:39', '0', 'a:1:{i:0;s:8:\"dd/list/\";}');
INSERT INTO 57sy_common_system_user VALUES ('9', '3334441', 'dcb64c94e1b81cd1cd3eb4a73ad27d99', '1', '1', '2014-03-01 15:27:01', '0', 'a:1:{i:0;s:3:\"555\";}');
INSERT INTO 57sy_common_system_user VALUES ('10', 'lili_', '44bc64daf3c37a7ada23e63a137310bd', '1', '3', '2014-03-02 10:05:43', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('11', 'yang', '44bc64daf3c37a7ada23e63a137310bd', '1', '3', '2014-03-02 10:05:53', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('12', 'feifei', '44bc64daf3c37a7ada23e63a137310bd', '1', '3', '2014-03-02 10:06:04', '0', '');
INSERT INTO 57sy_common_system_user VALUES ('13', '5567777', '3d2172418ce305c7d16d4b05597c6a59', '0', '2', '2014-03-03 03:35:51', '0', 'a:1:{i:0;s:11:\"role/index/\";}');

-- ----------------------------
-- Table structure for `57sy_common_user`
-- ----------------------------
DROP TABLE IF EXISTS `57sy_common_user`;
CREATE TABLE `57sy_common_user` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(15) NOT NULL COMMENT '用户名',
  `passwd` char(32) NOT NULL COMMENT '密码',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1正常0失效',
  `regdate` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册日期',
  `expire` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期日期 0永久',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 57sy_common_user
-- ----------------------------
INSERT INTO 57sy_common_user VALUES ('1', 'webuser', '44bc64daf3c37a7ada23e63a137310bd', '1', '1399082867', '0');
INSERT INTO 57sy_common_user VALUES ('2', 'taotao', '44bc64daf3c37a7ada23e63a137310bd', '1', '1399083755', '1399170120');
INSERT INTO 57sy_common_user VALUES ('3', 'haohao', '44bc64daf3c37a7ada23e63a137310bd', '1', '1399083888', '0');
INSERT INTO 57sy_common_user VALUES ('4', 'aojiao', 'd108ea09b955e95d68a9668a2b9c9443', '1', '1399083913', '1399861500');
INSERT INTO 57sy_common_user VALUES ('5', '1233', '78bf4f00f81a36b57950e239f1df91c1', '1', '1399085460', '0');
INSERT INTO 57sy_common_user VALUES ('6', '45666', 'b35b31a24acc2da3bd9e3feb30fc7e79', '1', '1399085469', '0');
INSERT INTO 57sy_common_user VALUES ('7', '十分丰富', '2aa11f3f451627b14158e812f5fd6ce7', '1', '1399085477', '0');
INSERT INTO 57sy_common_user VALUES ('8', '566yyy', '8523109c9a85dbfc46eb1f46955b5449', '1', '1399085486', '0');
INSERT INTO 57sy_common_user VALUES ('10', '5345', 'a2b45e7eaa7a1376c3fb1b13fd31620b', '1', '1399085657', '0');
INSERT INTO 57sy_common_user VALUES ('11', 'mytest', '11c5587af2863955b7c04c59a8732ccc', '1', '1399085666', '0');
INSERT INTO 57sy_common_user VALUES ('12', '45rere', 'fdf4511ac15862ff29a861c389dede90', '0', '1399085706', '0');
